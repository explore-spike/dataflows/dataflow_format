from setuptools import setup, find_packages

setup(
    name='dataflow_batch_retail_rl2_pl1',
    version='0.0.1',
    url='',
    license='MIT',
    author='Mehdi BEN HAJ ABBES',
    author_email='mehdi.abbes@gmail.com',
    description='Python template package to run batch dataflow jobs',
    packages=find_packages(),
    install_requires=[
        'apache-beam>=2.5.0'
    ],
    python_requires='~=2.7'
)

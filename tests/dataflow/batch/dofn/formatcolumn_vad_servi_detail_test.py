"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_detail = get_schema_from_json('resources/bq/DDL/schema/'
                                                       'sales_vad_servi_detail.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_detail(self):
        row_test = dict(catalog_price='0003250',
                        code_metier='VAD',
                        sequence_order='0000056',
                        ccr_price='',
                        qty_fact='001',
                        ncli='001425404',
                        ca_line='0001625',
                        pro_code='41047',
                        version_number=u'00003288',
                        line_status='9',
                        code_pays='FR',
                        use_code='1',
                        line_type='1',
                        source_file_name='servi_detail.txt.20190410044144',
                        code_marque='YR',
                        date_valeur='20190409',
                        orig_code='1',
                        code_flux='',
                        qty_delivered='001',
                        sequence_deliv='2',
                        processing_time='2019-04-10 15:31:20.955000')

        row = self.dofn.process(row_test, self.schema_dict_detail).next()

        self.assertEqual(row['catalog_price'], 3250.0)
        self.assertEqual(row['code_metier'], 'VAD')
        self.assertEqual(row['sequence_order'], 56)
        self.assertEqual(row['qty_fact'], 1)
        self.assertEqual(row['ncli'], '001425404')
        self.assertEqual(row['ca_line'], 1625.0)
        self.assertEqual(row['pro_code'], '41047')
        self.assertEqual(row['version_number'], 3288)
        self.assertEqual(row['line_status'], '9')
        self.assertEqual(row['code_pays'], 'FR')
        self.assertEqual(row['use_code'], '1')
        self.assertEqual(row['line_type'], '1')
        self.assertEqual(row['orig_code'], '1')
        self.assertEqual(row['date_valeur'], '2019-04-09')
        self.assertEqual(row['code_marque'], 'YR')
        self.assertEqual(row['source_file_name'], 'servi_detail.txt.20190410044144')
        self.assertEqual(row['qty_delivered'], 1)
        self.assertEqual(row['sequence_deliv'], 2)
        self.assertEqual(row['processing_time'], '2019-04-10 15:31:20.955000')

    # Tester si une valeur est vide
    def test_empty_values_detail(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(catalog_price='0003250',
                        code_metier='VAD',
                        sequence_order='0000056',
                        ccr_price='',
                        qty_fact='001',
                        ncli='001425404',
                        ca_line='0001625',
                        pro_code='41047',
                        version_number=u'00003288',
                        line_status='9',
                        code_pays='FR',
                        use_code='1',
                        line_type='1',
                        source_file_name='servi_detail.txt.20190410044144',
                        code_marque='YR',
                        date_valeur='20190409',
                        orig_code='1',
                        code_flux='',
                        qty_delivered='001',
                        sequence_deliv='2',
                        processing_time='2019-04-10 15:31:20.955000')

        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('code_flux' in row)
        self.assertFalse('ccr_price' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_detail(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(catalog_price='0003250',
                        code_metier='VAD',
                        sequence_order='0000056',
                        ccr_price='',
                        qty_fact='001',
                        ncli='001425404',
                        ca_line='0001625',
                        pro_code='41047',
                        version_number=u'00003288',
                        line_status='9',
                        code_pays='FR',
                        use_code='1',
                        line_type='1',
                        source_file_name='servi_detail.txt.20190410044144',
                        code_marque='YR',
                        date_valeur='20190409',
                        orig_code='1',
                        code_flux='',
                        qty_delivered='001',
                        sequence_deliv='2',
                        processing_time='2019-04-10 15:31:20.955000')

        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester type conforme
        self.assertIsInstance(row['catalog_price'], float)
        self.assertIsInstance(row['code_metier'], str)
        self.assertIsInstance(row['sequence_order'], int)
        self.assertIsInstance(row['qty_fact'], int)
        self.assertIsInstance(row['ncli'], str)
        self.assertIsInstance(row['ca_line'], float)
        self.assertIsInstance(row['pro_code'], str)
        self.assertIsInstance(row['version_number'], int)
        self.assertIsInstance(row['line_status'], str)
        self.assertIsInstance(row['code_pays'], str)
        self.assertIsInstance(row['use_code'], str)
        self.assertIsInstance(row['line_type'], str)
        self.assertIsInstance(row['orig_code'], str)
        self.assertIsInstance(row['date_valeur'], str)
        self.assertIsInstance(row['code_marque'], str)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['qty_delivered'], int)
        self.assertIsInstance(row['sequence_deliv'], int)
        self.assertIsInstance(row['processing_time'], str)

    def test_format_columns_detail_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (sequence_order='0000056B' alors que cette zone est integer et
            catalog_price='0003250X' alors que cette zone est float )
        :return:
        """
        row_test = dict(catalog_price='0003250X',
                        code_metier='VAD',
                        sequence_order='0000056B',
                        ccr_price='',
                        qty_fact='001',
                        ncli='001425404',
                        ca_line='0001625',
                        pro_code='41047',
                        version_number=u'00003288',
                        line_status='9',
                        code_pays='FR',
                        use_code='1',
                        line_type='1',
                        source_file_name='servi_detail.txt.20190410044144',
                        code_marque='YR',
                        date_valeur='20190409',
                        orig_code='1',
                        code_flux='',
                        qty_delivered='001',
                        sequence_deliv='2',
                        processing_time='2019-04-10 15:31:20.955000')

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_detail).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('sequence_order', '0000056B',
                                  'catalog_price', '0003250X'), list_logs)

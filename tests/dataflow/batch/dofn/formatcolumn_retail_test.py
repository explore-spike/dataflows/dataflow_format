"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_psd = get_schema_from_json('resources/bq/DDL/schema/'
                                                    'sales_retail_detail.json')
        self.schema_dict_psh = get_schema_from_json('resources/bq/DDL/schema/'
                                                    'sales_retail_header.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_psh(self):
        row_test = dict(psh_patch_rate='',
                        psh_service_vat_rate='20.00',
                        psh_shp_number='0507',
                        psh_trc_vpm='39.10',
                        psh_sale_date='20190205',
                        brd_code='YR',
                        psh_ticket_num='2',
                        psh_stand_alone='N',
                        psh_voucher_dscnt_rate='0.00',
                        psh_loc_promo='30.00',
                        psh_cashier_num='01',
                        psh_local_tax='',
                        psh_ticket_id='06N1903610002:10702',
                        psh_voucher_service_rate='0.00',
                        psh_sales_girl_code='02',
                        psh_federal_tax='',
                        psh_provincial_tax='',
                        processing_time='2019-02-11 08:50:05.131488 TC',
                        psh_currency='EUR',
                        cou_code='FR',
                        psh_typc_sales='1',
                        psh_mngmod_code='0',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190206010446.txt',
                        prefixe='PSH',
                        psh_customer_code='967313211',
                        psh_sale_time='0927')

        row = self.dofn.process(row_test, self.schema_dict_psh).next()

        self.assertEqual(row['psh_shp_number'], '0507')
        self.assertEqual(row['psh_service_vat_rate'], 20.00)
        self.assertEqual(row['psh_trc_vpm'], 39.10)
        self.assertEqual(row['psh_sale_date'], '2019-02-05')
        self.assertEqual(row['brd_code'], 'YR')
        self.assertEqual(row['psh_ticket_num'], 2)
        self.assertEqual(row['psh_stand_alone'], 'N')
        self.assertEqual(row['psh_voucher_dscnt_rate'], 0.00)
        self.assertEqual(row['psh_loc_promo'], 30.00)
        self.assertEqual(row['psh_cashier_num'], 1)
        self.assertEqual(row['psh_ticket_id'], '06N1903610002:10702')
        self.assertEqual(row['psh_voucher_service_rate'], 0.00)
        self.assertEqual(row['psh_sales_girl_code'], '02')
        self.assertEqual(row['psh_currency'], 'EUR')
        self.assertEqual(row['cou_code'], 'FR')
        self.assertEqual(row['psh_typc_sales'], '1')
        self.assertEqual(row['source_file_name'], 'VTE_LYR_SOC_YRFR_D_DET_20190206010446.txt')
        self.assertEqual(row['prefixe'], 'PSH')
        self.assertEqual(row['psh_customer_code'], '967313211')
        self.assertEqual(row['psh_sale_time'], '0927')

        # Tester si une valeur est vide

    def test_empty_values_psh(self):
        """

        :return:
        """
        row_test = dict(psh_patch_rate='',
                        psh_service_vat_rate='20.00',
                        psh_shp_number='0507',
                        psh_trc_vpm='39.10',
                        psh_sale_date='20190205',
                        brd_code='YR',
                        psh_ticket_num='2',
                        psh_stand_alone='N',
                        psh_voucher_dscnt_rate='0.00',
                        psh_loc_promo='30.00',
                        psh_cashier_num='01',
                        psh_local_tax='',
                        psh_ticket_id='06N1903610002:10702',
                        psh_voucher_service_rate='0.00',
                        psh_sales_girl_code='02',
                        psh_federal_tax='',
                        psh_provincial_tax='',
                        processing_time='2019-02-11 08:50:05.131488 TC',
                        psh_currency='EUR',
                        cou_code='FR',
                        psh_typc_sales='1',
                        psh_mngmod_code='0',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190206010446.txt',
                        prefixe='PSH',
                        psh_customer_code='967313211',
                        psh_sale_time='0927')
        row_test = self.dofn.process(row_test, self.schema_dict_psh)

        for row in row_test:
            self.assertFalse('psh_federal_tax' in row)
            self.assertFalse('psh_provincial_tax' in row)
            self.assertFalse('psh_local_tax' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_psh_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (psh_service_vat_rate='A' alors que cette zone est float)
        :return:
        """
        row_test = dict(psh_patch_rate='',
                        psh_service_vat_rate='A',
                        psh_shp_number='0507',
                        psh_trc_vpm='39.10',
                        psh_sale_date='20190205',
                        brd_code='YR',
                        psh_ticket_num='2',
                        psh_stand_alone='N',
                        psh_voucher_dscnt_rate='b',
                        psh_loc_promo='30.00',
                        psh_cashier_num='01',
                        psh_local_tax='',
                        psh_ticket_id='06N1903610002:10702',
                        psh_voucher_service_rate='0.00',
                        psh_sales_girl_code='02',
                        psh_federal_tax='',
                        psh_provincial_tax='',
                        processing_time='2019-02-11 08:50:05.131488 TC',
                        psh_currency='EUR',
                        cou_code='FR',
                        psh_typc_sales='1',
                        psh_mngmod_code='0',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190206010446.txt',
                        prefixe='PSH',
                        psh_customer_code='967313211',
                        psh_sale_time='0927')

        # on teste si y a  bien un message d en cas de mauvais format de donnees
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_psh).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" '
                          'and ''Format error on column "{}" '
                                'with bad value "{}"'.format('psh_service_vat_rate',
                                                             'A',
                                                             'psh_voucher_dscnt_rate',
                                                             'b', ), list_logs)

    def test_format_columns_psd(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(prefixe='PSD',
                        brd_code='YR',
                        cou_code='FR',
                        psh_ticket_id='0EX1902010001:28288',
                        psd_line_nb='2',
                        psd_typc_sales='1',
                        psd_mvt_type='V04',
                        psd_mvt_code='31',
                        psd_sales_girl_code='07',
                        psd_customer_code='659523687',
                        psd_pro_code='75095',
                        psd_offer_code='J902',
                        psd_suboffer_code='061363',
                        psd_cp_code='372299',
                        psd_key_code='',
                        psd_vat_value='20.00',
                        psd_discount_type='1',
                        psd_etrc='0.00',
                        psd_product_discount='0.00',
                        psd_local_promo='N',
                        psd_sat_code='24',
                        psd_federal_tax='',
                        psd_provincial_tax='',
                        psd_local_tax='',
                        psd_qty='1',
                        psd_cat_price='4.95',
                        psd_cat_price_tf='4.13',
                        psd_proposed_price='2.48',
                        psd_proposed_price_tf='2.07',
                        psd_sale_price='2.48',
                        psd_sale_price_tf='2.07',
                        psd_sale_netprice='2.48',
                        psd_sale_netprice_tf='2.07',
                        psd_exclusion_ca='0',
                        psd_compo_code='',
                        psd_sbs_code='',
                        psd_sbs_purch_flag='0',
                        psd_ticket_end_use='0',
                        psd_line_yr_discount='2.47',
                        psd_line_yr_discount_tf='2.06',
                        psd_line_fr_discount='0.00',
                        psd_line_fr_discount_tf='0.00',
                        psd_eot_yr_discount='0.00',
                        psd_eot_yr_discount_tf='0.00',
                        psd_eot_fr_discount='0.00',
                        psd_eot_fr_discount_tf='0.00',
                        psd_line_gross_voucher_amt='2.47',
                        psd_line_gross_voucher_amt_tf='2.06',
                        psd_line_net_voucher_amt='1.56',
                        psd_line_net_voucher_amt_tf='1.30',
                        psd_eot_gross_voucher_amt='0.00',
                        psd_eot_gross_voucher_amt_tf='0.00',
                        psd_eot_net_voucher_amt='0.00',
                        psd_eot_net_voucher_amt_tf='0.00',
                        psd_partner_commission='0.77',
                        psd_recargo_rate='0.000000',
                        psd_sof_fidelity_flag='N',
                        psd_sof_yr_care_amount='',
                        psd_yr_care_discount='0.00',
                        psd_return_control_flag='',
                        psd_return_motif='',
                        psd_cat_code='1',
                        psd_sbs_code_prog='',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt',
                        processing_time='2019-02-13 10:16:41.419 UTC')

        rows_generator = self.dofn.process(row_test, self.schema_dict_psd)

        row = rows_generator.next()

        # Tester type conforme
        self.assertIsInstance(row['prefixe'], str)
        self.assertIsInstance(row['brd_code'], str)
        self.assertIsInstance(row['cou_code'], str)
        self.assertIsInstance(row['psh_ticket_id'], str)
        self.assertIsInstance(row['psd_line_nb'], int)
        self.assertIsInstance(row['psd_typc_sales'], str)
        self.assertIsInstance(row['psd_mvt_type'], str)
        self.assertIsInstance(row['psd_mvt_code'], int)
        self.assertIsInstance(row['psd_sales_girl_code'], str)
        self.assertIsInstance(row['psd_customer_code'], str)
        self.assertIsInstance(row['psd_pro_code'], str)
        self.assertIsInstance(row['psd_offer_code'], str)
        self.assertIsInstance(row['psd_suboffer_code'], str)
        self.assertIsInstance(row['psd_cp_code'], int)
        self.assertIsInstance(row['psd_vat_value'], float)
        self.assertIsInstance(row['psd_discount_type'], int)
        self.assertIsInstance(row['psd_etrc'], float)
        self.assertIsInstance(row['psd_product_discount'], float)
        self.assertIsInstance(row['psd_local_promo'], str)
        self.assertIsInstance(row['psd_sat_code'], str)
        self.assertIsInstance(row['psd_qty'], int)
        self.assertIsInstance(row['psd_cat_price'], float)
        self.assertIsInstance(row['psd_cat_price_tf'], float)
        self.assertIsInstance(row['psd_proposed_price'], float)
        self.assertIsInstance(row['psd_proposed_price_tf'], float)
        self.assertIsInstance(row['psd_sale_price'], float)
        self.assertIsInstance(row['psd_sale_price_tf'], float)
        self.assertIsInstance(row['psd_sale_netprice'], float)
        self.assertIsInstance(row['psd_sale_netprice_tf'], float)
        self.assertIsInstance(row['psd_exclusion_ca'], int)
        self.assertIsInstance(row['psd_sbs_purch_flag'], int)
        self.assertIsInstance(row['psd_ticket_end_use'], str)
        self.assertIsInstance(row['psd_line_yr_discount'], float)
        self.assertIsInstance(row['psd_line_yr_discount_tf'], float)
        self.assertIsInstance(row['psd_line_fr_discount'], float)
        self.assertIsInstance(row['psd_line_fr_discount_tf'], float)
        self.assertIsInstance(row['psd_eot_yr_discount'], float)
        self.assertIsInstance(row['psd_eot_yr_discount_tf'], float)
        self.assertIsInstance(row['psd_eot_fr_discount'], float)
        self.assertIsInstance(row['psd_eot_fr_discount_tf'], float)
        self.assertIsInstance(row['psd_line_gross_voucher_amt'], float)
        self.assertIsInstance(row['psd_line_gross_voucher_amt_tf'], float)
        self.assertIsInstance(row['psd_line_net_voucher_amt'], float)
        self.assertIsInstance(row['psd_line_net_voucher_amt_tf'], float)
        self.assertIsInstance(row['psd_eot_gross_voucher_amt'], float)
        self.assertIsInstance(row['psd_eot_gross_voucher_amt_tf'], float)
        self.assertIsInstance(row['psd_eot_net_voucher_amt'], float)
        self.assertIsInstance(row['psd_eot_net_voucher_amt_tf'], float)
        self.assertIsInstance(row['psd_partner_commission'], float)
        self.assertIsInstance(row['psd_recargo_rate'], float)
        self.assertIsInstance(row['psd_sof_fidelity_flag'], str)
        self.assertIsInstance(row['psd_yr_care_discount'], float)
        self.assertIsInstance(row['psd_cat_code'], str)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['processing_time'], str)

    def test_values_columns_psd(self):
        """
            on teste les valeurs des colonnes de l'enregistrement :
            toutes les colonnes doivent avoir une valeur conforme
        :return:
        """
        row_test = dict(prefixe='PSD',
                        brd_code='YR',
                        cou_code='FR',
                        psh_ticket_id='0EX1902010001:28288',
                        psd_line_nb='2',
                        psd_typc_sales='1',
                        psd_mvt_type='V04',
                        psd_mvt_code='31',
                        psd_sales_girl_code='07',
                        psd_customer_code='659523687',
                        psd_pro_code='75095',
                        psd_offer_code='J902',
                        psd_suboffer_code='061363',
                        psd_cp_code='372299',
                        psd_key_code='',
                        psd_vat_value='20.00',
                        psd_discount_type='1',
                        psd_etrc='0.00',
                        psd_product_discount='0.00',
                        psd_local_promo='N',
                        psd_sat_code='24',
                        psd_federal_tax='',
                        psd_provincial_tax='',
                        psd_local_tax='',
                        psd_qty='1',
                        psd_cat_price='4.95',
                        psd_cat_price_tf='4.13',
                        psd_proposed_price='2.48',
                        psd_proposed_price_tf='2.07',
                        psd_sale_price='2.48',
                        psd_sale_price_tf='2.07',
                        psd_sale_netprice='2.48',
                        psd_sale_netprice_tf='2.07',
                        psd_exclusion_ca='0',
                        psd_compo_code='',
                        psd_sbs_code='',
                        psd_sbs_purch_flag='0',
                        psd_ticket_end_use='0',
                        psd_line_yr_discount='2.47',
                        psd_line_yr_discount_tf='2.06',
                        psd_line_fr_discount='0.00',
                        psd_line_fr_discount_tf='0.00',
                        psd_eot_yr_discount='0.00',
                        psd_eot_yr_discount_tf='0.00',
                        psd_eot_fr_discount='0.00',
                        psd_eot_fr_discount_tf='0.00',
                        psd_line_gross_voucher_amt='2.47',
                        psd_line_gross_voucher_amt_tf='2.06',
                        psd_line_net_voucher_amt='1.56',
                        psd_line_net_voucher_amt_tf='1.30',
                        psd_eot_gross_voucher_amt='0.00',
                        psd_eot_gross_voucher_amt_tf='0.00',
                        psd_eot_net_voucher_amt='0.00',
                        psd_eot_net_voucher_amt_tf='0.00',
                        psd_partner_commission='0.77',
                        psd_recargo_rate='0.000000',
                        psd_sof_fidelity_flag='N',
                        psd_sof_yr_care_amount='',
                        psd_yr_care_discount='0.00',
                        psd_return_control_flag='',
                        psd_return_motif='',
                        psd_cat_code='1',
                        psd_sbs_code_prog='',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt',
                        processing_time='2019-02-13 10:16:41.419 UTC')

        rows_generator = self.dofn.process(row_test, self.schema_dict_psd)

        row = rows_generator.next()

        # Tester valeur
        self.assertEqual(row['prefixe'], 'PSD')
        self.assertEqual(row['brd_code'], 'YR')
        self.assertEqual(row['cou_code'], 'FR')
        self.assertEqual(row['psh_ticket_id'], '0EX1902010001:28288')
        self.assertEqual(row['psd_line_nb'], 2)
        self.assertEqual(row['psd_typc_sales'], '1')
        self.assertEqual(row['psd_mvt_type'], 'V04')
        self.assertEqual(row['psd_mvt_code'], 31)
        self.assertEqual(row['psd_sales_girl_code'], '07')
        self.assertEqual(row['psd_customer_code'], '659523687')
        self.assertEqual(row['psd_pro_code'], '75095')
        self.assertEqual(row['psd_offer_code'], 'J902')
        self.assertEqual(row['psd_suboffer_code'], '061363')
        self.assertEqual(row['psd_cp_code'], 372299)
        self.assertEqual(row['psd_vat_value'], 20.00)
        self.assertEqual(row['psd_discount_type'], 1)
        self.assertEqual(row['psd_etrc'], 0.00)
        self.assertEqual(row['psd_product_discount'], 0.00)
        self.assertEqual(row['psd_local_promo'], 'N')
        self.assertEqual(row['psd_sat_code'], '24')
        self.assertEqual(row['psd_qty'], 1)
        self.assertEqual(row['psd_cat_price'], 4.95)
        self.assertEqual(row['psd_cat_price_tf'], 4.13)
        self.assertEqual(row['psd_proposed_price'], 2.48)
        self.assertEqual(row['psd_proposed_price_tf'], 2.07)
        self.assertEqual(row['psd_sale_price'], 2.48)
        self.assertEqual(row['psd_sale_price_tf'], 2.07)
        self.assertEqual(row['psd_sale_netprice'], 2.48)
        self.assertEqual(row['psd_sale_netprice_tf'], 2.07)
        self.assertEqual(row['psd_exclusion_ca'], 0)
        self.assertEqual(row['psd_sbs_purch_flag'], 0)
        self.assertEqual(row['psd_ticket_end_use'], '0')
        self.assertEqual(row['psd_line_yr_discount'], 2.47)
        self.assertEqual(row['psd_line_yr_discount_tf'], 2.06)
        self.assertEqual(row['psd_line_fr_discount'], 0.00)
        self.assertEqual(row['psd_line_fr_discount_tf'], 0.00)
        self.assertEqual(row['psd_eot_yr_discount'], 0.00)
        self.assertEqual(row['psd_eot_yr_discount_tf'], 0.00)
        self.assertEqual(row['psd_eot_fr_discount'], 0.00)
        self.assertEqual(row['psd_eot_fr_discount_tf'], 0.00)
        self.assertEqual(row['psd_line_gross_voucher_amt'], 2.47)
        self.assertEqual(row['psd_line_gross_voucher_amt_tf'], 2.06)
        self.assertEqual(row['psd_line_net_voucher_amt'], 1.56)
        self.assertEqual(row['psd_line_net_voucher_amt_tf'], 1.30)
        self.assertEqual(row['psd_eot_gross_voucher_amt'], 0.00)
        self.assertEqual(row['psd_eot_gross_voucher_amt_tf'], 0.00)
        self.assertEqual(row['psd_eot_net_voucher_amt'], 0.00)
        self.assertEqual(row['psd_eot_net_voucher_amt_tf'], 0.00)
        self.assertEqual(row['psd_partner_commission'], 0.77)
        self.assertEqual(row['psd_recargo_rate'], 0.000000)
        self.assertEqual(row['psd_sof_fidelity_flag'], 'N')
        self.assertEqual(row['psd_yr_care_discount'], 0.00)
        self.assertEqual(row['psd_cat_code'], '1')
        self.assertEqual(row['source_file_name'], 'VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt')
        self.assertEqual(row['processing_time'], '2019-02-13 10:16:41.419 UTC')

    def test_empty_values_psd(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(prefixe='PSD',
                        brd_code='YR',
                        cou_code='FR',
                        psh_ticket_id='0EX1902010001:28288',
                        psd_line_nb='2',
                        psd_typc_sales='1',
                        psd_mvt_type='V04',
                        psd_mvt_code='31',
                        psd_sales_girl_code='07',
                        psd_customer_code='659523687',
                        psd_pro_code='75095',
                        psd_offer_code='J902',
                        psd_suboffer_code='061363',
                        psd_cp_code='372299',
                        psd_key_code='',
                        psd_vat_value='20.00',
                        psd_discount_type='1',
                        psd_etrc='0.00',
                        psd_product_discount='0.00',
                        psd_local_promo='N',
                        psd_sat_code='24',
                        psd_federal_tax='',
                        psd_provincial_tax='',
                        psd_local_tax='',
                        psd_qty='1',
                        psd_cat_price='4.95',
                        psd_cat_price_tf='4.13',
                        psd_proposed_price='2.48',
                        psd_proposed_price_tf='2.07',
                        psd_sale_price='2.48',
                        psd_sale_price_tf='2.07',
                        psd_sale_netprice='2.48',
                        psd_sale_netprice_tf='2.07',
                        psd_exclusion_ca='0',
                        psd_compo_code='',
                        psd_sbs_code='',
                        psd_sbs_purch_flag='0',
                        psd_ticket_end_use='0',
                        psd_line_yr_discount='2.47',
                        psd_line_yr_discount_tf='2.06',
                        psd_line_fr_discount='0.00',
                        psd_line_fr_discount_tf='0.00',
                        psd_eot_yr_discount='0.00',
                        psd_eot_yr_discount_tf='0.00',
                        psd_eot_fr_discount='0.00',
                        psd_eot_fr_discount_tf='0.00',
                        psd_line_gross_voucher_amt='2.47',
                        psd_line_gross_voucher_amt_tf='2.06',
                        psd_line_net_voucher_amt='1.56',
                        psd_line_net_voucher_amt_tf='1.30',
                        psd_eot_gross_voucher_amt='0.00',
                        psd_eot_gross_voucher_amt_tf='0.00',
                        psd_eot_net_voucher_amt='0.00',
                        psd_eot_net_voucher_amt_tf='0.00',
                        psd_partner_commission='0.77',
                        psd_recargo_rate='0.000000',
                        psd_sof_fidelity_flag='N',
                        psd_sof_yr_care_amount='',
                        psd_yr_care_discount='0.00',
                        psd_return_control_flag='',
                        psd_return_motif='',
                        psd_cat_code='1',
                        psd_sbs_code_prog='',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt',
                        processing_time='2019-02-13 10:16:41.419 UTC')

        rows_generator = self.dofn.process(row_test, self.schema_dict_psd)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('psd_key_code' in row)
        self.assertFalse('psd_federal_tax' in row)
        self.assertFalse('psd_provincial_tax' in row)
        self.assertFalse('psd_local_tax' in row)
        self.assertFalse('psd_compo_code' in row)
        self.assertFalse('psd_sbs_code' in row)
        self.assertFalse('psd_sof_yr_care_amount' in row)
        self.assertFalse('psd_return_control_flag' in row)
        self.assertFalse('psd_return_motif' in row)
        self.assertFalse('psd_sbs_code_prog' in row)

    def test_format_columns_psd_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (psd_line_nb='2B' alors que cette zone est integer et
            psd_sale_price_tf='2.B07' alors que cette zone est float )
        :return:
        """
        row_test = dict(prefixe='PSD',
                        brd_code='YR',
                        cou_code='FR',
                        psh_ticket_id='0EX1902010001:28288',
                        psd_line_nb='2B',
                        psd_typc_sales='1',
                        psd_mvt_type='V04',
                        psd_mvt_code='31',
                        psd_sales_girl_code='07',
                        psd_customer_code='659523687',
                        psd_pro_code='75095',
                        psd_offer_code='J902',
                        psd_suboffer_code='061363',
                        psd_cp_code='372299',
                        psd_key_code='',
                        psd_vat_value='20.00',
                        psd_discount_type='1',
                        psd_etrc='0.00',
                        psd_product_discount='0.00',
                        psd_local_promo='N',
                        psd_sat_code='24',
                        psd_federal_tax='',
                        psd_provincial_tax='',
                        psd_local_tax='',
                        psd_qty='1',
                        psd_cat_price='4.95',
                        psd_cat_price_tf='4.13',
                        psd_proposed_price='2.48',
                        psd_proposed_price_tf='2.07',
                        psd_sale_price='2.48',
                        psd_sale_price_tf='2.B07',
                        psd_sale_netprice='2.48',
                        psd_sale_netprice_tf='2.07',
                        psd_exclusion_ca='0',
                        psd_compo_code='',
                        psd_sbs_code='',
                        psd_sbs_purch_flag='0',
                        psd_ticket_end_use='0',
                        psd_line_yr_discount='2.47',
                        psd_line_yr_discount_tf='2.06',
                        psd_line_fr_discount='0.00',
                        psd_line_fr_discount_tf='0.00',
                        psd_eot_yr_discount='0.00',
                        psd_eot_yr_discount_tf='0.00',
                        psd_eot_fr_discount='0.00',
                        psd_eot_fr_discount_tf='0.00',
                        psd_line_gross_voucher_amt='2.47',
                        psd_line_gross_voucher_amt_tf='2.06',
                        psd_line_net_voucher_amt='1.56',
                        psd_line_net_voucher_amt_tf='1.30',
                        psd_eot_gross_voucher_amt='0.00',
                        psd_eot_gross_voucher_amt_tf='0.00',
                        psd_eot_net_voucher_amt='0.00',
                        psd_eot_net_voucher_amt_tf='0.00',
                        psd_partner_commission='0.77',
                        psd_recargo_rate='0.000000',
                        psd_sof_fidelity_flag='N',
                        psd_sof_yr_care_amount='',
                        psd_yr_care_discount='0.00',
                        psd_return_control_flag='',
                        psd_return_motif='',
                        psd_cat_code='1',
                        psd_sbs_code_prog='',
                        source_file_name='VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt',
                        processing_time='2019-02-13 10:16:41.419 UTC')

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_psd).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('psd_line_nb', '2B', 'psd_sale_price_tf', '2.B07'), list_logs)

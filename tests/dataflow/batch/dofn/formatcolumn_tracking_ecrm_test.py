"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer  2
    """

    def setUp(self):
        self.schema_dict = get_schema_from_json('resources/bq/DDL/schema/tracking_ecrm.json')
        self.dofn = FormatColumnDoFn()
        self.row_test = dict(
            ibroadlogid=2067573074,
            ideliveryid=1331040081,
            istatus=1,
            tsevent="2019-05-27 12:18:03",
            itrackinglogid=693390975,
            iurlid=1,
            irecipientid=12956991,
            semail="8199e87fba2285944260425f9449250771fa0ca10f255f40f08331585eeaf521",
            sdeletedreason='',
            tsdelete='',
            ioperationid='',
            itype=1,
            damount='',
            bcc_rgpt_code="410488094",
            source_file_name='EXTRACT_TRACKING_ECRM_GCP_YRFR.txt',
            processing_time='2019-05-13 15:31:20.955000'
        )

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row = self.dofn.process(self.row_test, self.schema_dict).next()

        self.assertEqual(row['ibroadlogid'], 2067573074)
        self.assertEqual(row['ideliveryid'], 1331040081)
        self.assertEqual(row['istatus'], 1)
        self.assertEqual(row['tsevent'], "2019-05-27 12:18:03")
        self.assertEqual(row['itrackinglogid'], 693390975)
        self.assertEqual(row['iurlid'], 1)
        self.assertEqual(row['irecipientid'], 12956991)
        self.assertEqual(row['semail'], "8199e87fba2285944260425f9449250771fa0ca10f255f40f08331585eeaf521")
        self.assertEqual(row['itype'], 1)
        self.assertEqual(row['bcc_rgpt_code'], "410488094")

    # Tester si une valeur est vide
    def test_empty_values(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """

        rows_generator = self.dofn.process(self.row_test, self.schema_dict)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('sdeletedreason' in row)
        self.assertFalse('tsdelete' in row)
        self.assertFalse('ioperationid' in row)
        self.assertFalse('damount' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns(self):
        """
            test d'un enregistrement qui ne passe pas
            (brd_sk='1x' alors que cette zone est integer et cou_sk=2X)
        :return:
        """
        row_test = dict(
            ibroadlogid=2067573074,
            ideliveryid='1331040081e',
            istatus=1,
            tsevent="2019-05-27 12:18:03",
            itrackinglogid=693390975,
            iurlid=1,
            irecipientid='1295699e1',
            semail="8199e87fba2285944260425f9449250771fa0ca10f255f40f08331585eeaf521",
            sdeletedreason='',
            tsdelete='',
            ioperationid='',
            itype=1,
            damount='',
            bcc_rgpt_code="410488094",
            source_file_name='EXTRACT_TRACKING_ECRM_GCP_YRFR.txt',
            processing_time='2019-05-13 15:31:20.955000'
        )

        # on teste si le retour dans le login comporte bien un message d'erreur

        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('ideliveryid', '1331040081e', 'irecipientid', '1295699e1'), list_logs)

"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
        YR|FR|4757|1|0074|2019-05-20 14:45:29|P01|2017-10-24 00:00:00|1900-01-01 7::00|1900-01-01 00:00:00|0|0|1
    """

    def setUp(self):
        self.schema_dict_ = get_schema_from_json('resources/bq/DDL/schema/traffic_retail.json')

        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row_test = dict(
            brd_code='YR',

            cou_code='FR',

            fix_internal_id='4757',

            fix_tlo_seq='1',

            ppc_shp_number='0074',

            ppc_creation_date='2019-05-20 14:45:29',

            ppc_ref_portail='P01',

            ppc_date_debut='2017-10-24 00:00:00',

            ppc_heure_debut='1900-01-01 07:00:00',

            ppc_duree='1900-01-01 00:00:00',

            ppc_qty_in='0',

            ppc_qty_out='0',

            ppc_pi_flag='1',

            source_file_name='CPT.txt',

            processing_time='2017-10-24 00:00:00',

            version_number='0001'
        )

        parsed_line = self.dofn.process(row_test, self.schema_dict_).next()
        print parsed_line

        self.assertEqual(parsed_line['brd_code'], 'YR')
        self.assertEqual(parsed_line['cou_code'], 'FR')
        self.assertEqual(parsed_line['fix_internal_id'], 4757)
        self.assertEqual(parsed_line['fix_tlo_seq'], 1)
        self.assertEqual(parsed_line['ppc_shp_number'], '0074')
        self.assertEqual(parsed_line['ppc_creation_date'], '2019-05-20 14:45:29')
        self.assertEqual(parsed_line['ppc_ref_portail'], 'P01')
        self.assertEqual(parsed_line['ppc_date_debut'], '2017-10-24 00:00:00')
        self.assertEqual(parsed_line['ppc_heure_debut'], '1900-01-01 07:00:00')
        self.assertEqual(parsed_line['ppc_duree'], '1900-01-01 00:00:00')
        self.assertEqual(parsed_line['ppc_qty_in'], 0)
        self.assertEqual(parsed_line['ppc_qty_out'], 0)
        self.assertEqual(parsed_line['ppc_pi_flag'], '1')
        self.assertEqual(parsed_line['source_file_name'], 'CPT.txt')
        self.assertEqual(parsed_line['processing_time'], '2017-10-24 00:00:00')
        self.assertEqual(parsed_line['version_number'], '0001')



    def test_format_columns(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(
            brd_code='YR',

            cou_code='FR',

            fix_internal_id='4757',

            fix_tlo_seq='1',

            ppc_shp_number='0074',

            ppc_creation_date='2019-05-20 14:45:29',

            ppc_ref_portail='P01',

            ppc_date_debut='2017-10-24 00:00:00',

            ppc_heure_debut='1900-01-01 7:00:00',

            ppc_duree='1900-01-01 00:00:00',

            ppc_qty_in='0',

            ppc_qty_out='0',

            ppc_pi_flag='1',

            source_file_name='CPT.txt',

            processing_time='2017-10-24 00:00:00',

            version_number='0001'
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_)

        row = rows_generator.next()
        print self.schema_dict_
        # Tester type conforme
        self.assertIsInstance(row['brd_code'], str)
        self.assertIsInstance(row['cou_code'], str)
        self.assertIsInstance(row['fix_internal_id'], int)
        self.assertIsInstance(row['fix_tlo_seq'], int)
        self.assertIsInstance(row['ppc_shp_number'], str)
        self.assertIsInstance(row['ppc_creation_date'], str)
        self.assertIsInstance(row['ppc_ref_portail'], str)
        self.assertIsInstance(row['ppc_date_debut'], str)
        self.assertIsInstance(row['ppc_heure_debut'], str)
        self.assertIsInstance(row['ppc_duree'], str)
        self.assertIsInstance(row['ppc_qty_in'], int)
        self.assertIsInstance(row['ppc_qty_out'], int)
        self.assertIsInstance(row['ppc_pi_flag'], str)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['processing_time'], str)
        self.assertIsInstance(row['version_number'], str)

    def test_format_columns_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (psh_tva_prestation='B' alors que cette zone est float et
            psh_voucher_dscnt_rate='A' alors que cette zone est float )
        :return:
        """
        row_test = dict(
            brd_code='YR',

            cou_code='FR',

            fix_internal_id='4757',

            fix_tlo_seq='1',

            ppc_shp_number='0074',

            ppc_creation_date='2019-05-20 14:45:29',

            ppc_ref_portail='P01',

            ppc_date_debut='2017-10-24 00:00:00',

            ppc_heure_debut='1900-01-01 7:00:00',

            ppc_duree='1900-01-01 00:00:00',

            ppc_qty_in='A',

            ppc_qty_out='B',

            ppc_pi_flag='1',

            source_file_name='CPT.txt',

            processing_time='2017-10-24 00:00:00',

            version_number='0001'
        )

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}"'
                          ' with bad value "{}"'.format('ppc_qty_in',
                                                        'A', 'ppc_qty_out', 'B',
                                                        ), list_logs)

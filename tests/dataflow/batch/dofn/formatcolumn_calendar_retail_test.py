"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_ = get_schema_from_json('resources/bq/DDL/schema/calendar_retail.json')

        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row_test = dict(
            prefixe='TOBJ1',

            tcal1_date_n='',

            tcal1_date_n_1='',

            tcal1_system_last_update='',

            tcal1_cou_code='',

            tfer1_shp_number='',

            tfer1_cou_code='',

            tfer1_public_holiday_date='',

            tfer1_system_last_update='',

            ttse1_cou_code='',

            ttse1_ea_type_code='',

            ttse1_ea_type_label='',

            ttse1_system_last_update='',

            tsem1_shp_number='',

            tsem1_cou_code='',

            tsem1_ea_type_code='',

            tsem1_ea_date='',

            tsem1_ea_month_flag='',

            tsem1_ea_month_day='',

            tsem1_system_last_update='',

            tobj1_cou_code='FR',

            tobj1_shp_number='0033',

            tobj1_abp_abpdate='20180718',

            tobj1_abp_cayrcom="3215.81",

            tobj1_abp_cayrobj="3215.81",

            tobj1_abp_cayrbud="3109.15",

            tobj1_abp_debyrcom="183.0",

            tobj1_abp_debyrobj="183.0",

            tobj1_abp_debyrbud="176.0",

            tobj1_system_last_update='20180718',

            tobj1_abp_cayrcare="0.0",

            tobj1_abp_cayrcaused="0.0",

            tobj1_abp_cayrspont="0.0",

            tobj1_abp_cayrtotal="0.0",

            tobj1_abp_debyrcare="0.0",

            tobj1_abp_debyrcaused="0.0",

            tobj1_abp_debyrspont="0.0",

            tobj1_abp_debyrtotal="0.0",

            source_file_name='REF_TOS_LYR_YRFR_B_TOS_20190605145100.txt',

            processing_time='2019-06-06 11:29:32.838598 UTC',

            version_number='0001'
        )

        parsed_line = self.dofn.process(row_test, self.schema_dict_).next()

        self.assertEqual(parsed_line['prefixe'], 'TOBJ1')
        self.assertEqual(parsed_line['tobj1_cou_code'], 'FR')
        self.assertEqual(parsed_line['tobj1_shp_number'], '0033')
        self.assertEqual(parsed_line['tobj1_abp_abpdate'], '2018-07-18')
        self.assertEqual(parsed_line['tobj1_abp_debyrbud'], 176.0)
        self.assertEqual(parsed_line['tobj1_abp_debyrtotal'], 0.0)
        self.assertEqual(parsed_line['version_number'], '0001')



    def test_format_columns(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(
            prefixe='TOBJ1',

            tcal1_date_n='',

            tcal1_date_n_1='',

            tcal1_system_last_update='',

            tcal1_cou_code='',

            tfer1_shp_number='',

            tfer1_cou_code='',

            tfer1_public_holiday_date='',

            tfer1_system_last_update='',

            ttse1_cou_code='',

            ttse1_ea_type_code='',

            ttse1_ea_type_label='',

            ttse1_system_last_update='',

            tsem1_shp_number='',

            tsem1_cou_code='',

            tsem1_ea_type_code='',

            tsem1_ea_date='',

            tsem1_ea_month_flag='',

            tsem1_ea_month_day='',

            tsem1_system_last_update='',

            tobj1_cou_code='FR',

            tobj1_shp_number='0033',

            tobj1_abp_abpdate='20180718',

            tobj1_abp_cayrcom="3215.81",

            tobj1_abp_cayrobj="3215.81",

            tobj1_abp_cayrbud="3109.15",

            tobj1_abp_debyrcom="183.0",

            tobj1_abp_debyrobj="183.0",

            tobj1_abp_debyrbud='176.0',

            tobj1_system_last_update='20180718',

            tobj1_abp_cayrcare="0.0",

            tobj1_abp_cayrcaused="0.0",

            tobj1_abp_cayrspont="0.0",

            tobj1_abp_cayrtotal="0.0",

            tobj1_abp_debyrcare="0.0",

            tobj1_abp_debyrcaused="0.0",

            tobj1_abp_debyrspont="0.0",

            tobj1_abp_debyrtotal="0.0",

            source_file_name='REF_TOS_LYR_YRFR_B_TOS_20190605145100.txt',

            processing_time='2019-06-06 11:29:32.838598 UTC',

            version_number='0001'
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_)

        row = rows_generator.next()
        print self.schema_dict_
        # Tester type conforme
        self.assertIsInstance(row['prefixe'], str)
        self.assertIsInstance(row['tobj1_cou_code'], str)
        self.assertIsInstance(row['tobj1_shp_number'], str)
        self.assertIsInstance(row['tobj1_abp_abpdate'], str)
        self.assertIsInstance(row['tobj1_abp_debyrbud'], float)
        self.assertIsInstance(row['tobj1_abp_debyrtotal'], float)
        self.assertIsInstance(row['version_number'], str)

    def test_format_columns_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (psh_tva_prestation='B' alors que cette zone est float et
            psh_voucher_dscnt_rate='A' alors que cette zone est float )
        :return:
        """
        row_test = dict(
            prefixe='TOBJ1',

            tcal1_date_n='',

            tcal1_date_n_1='',

            tcal1_system_last_update='',

            tcal1_cou_code='',

            tfer1_shp_number='',

            tfer1_cou_code='',

            tfer1_public_holiday_date='',

            tfer1_system_last_update='',

            ttse1_cou_code='',

            ttse1_ea_type_code='',

            ttse1_ea_type_label='',

            ttse1_system_last_update='',

            tsem1_shp_number='',

            tsem1_cou_code='',

            tsem1_ea_type_code='',

            tsem1_ea_date='',

            tsem1_ea_month_flag='',

            tsem1_ea_month_day='',

            tsem1_system_last_update='',

            tobj1_cou_code='FR',

            tobj1_shp_number='0033',

            tobj1_abp_abpdate='20180718',

            tobj1_abp_cayrcom='3215.81',

            tobj1_abp_cayrobj='3215.81',

            tobj1_abp_cayrbud='3109.15',

            tobj1_abp_debyrcom='183.0',

            tobj1_abp_debyrobj='A',

            tobj1_abp_debyrbud='B',

            tobj1_system_last_update='20190909',

            tobj1_abp_cayrcare='0.0',

            tobj1_abp_cayrcaused='0.0',

            tobj1_abp_cayrspont='0.0',

            tobj1_abp_cayrtotal='0.0',

            tobj1_abp_debyrcare='0.0',

            tobj1_abp_debyrcaused='0.0',

            tobj1_abp_debyrspont='0.0',

            tobj1_abp_debyrtotal='0.0',

            source_file_name='REF_TOS_LYR_YRFR_B_TOS_20190605145100.txt',

            processing_time='2019-06-06 11:29:32.838598 UTC',

            version_number='0001')

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}"'
                          ' with bad value "{}"'.format('tobj1_abp_debyrobj',
                                                        'A', 'tobj1_abp_debyrbud', 'B',
                                                        ), list_logs)

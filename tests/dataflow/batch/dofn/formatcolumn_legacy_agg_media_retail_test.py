"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer  2
    """

    def setUp(self):
        self.schema_dict_prospection = get_schema_from_json('resources/bq/DDL/schema/'
                                                       'legacy_agg_media_retail.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row_test = dict(
            cst_sk='5013388254',
            cst_code='879304016',
            swp_sk='1',
            swp_code='[14],[27],[46]',
            time_sk='6752',
            sale_date='20150617',
            swp_day_pdt_sent_nbr='0',
            swp_day_pdt_ret_nbr='1',
            swp_day_pdt_yr_purch_nbr='1',
            swp_day_pdt_yr_tvr='4.45',
            swp_day_care_tvr='0.00',
            system_last_update ='20150626 12:06:59',
            source_file_name ='SWP_ALZ_GCP_YRFR_D_DET.txt',
            processing_time ='2019-06-24 11:40:29.962555 UTC',
            version_number ='201905'
        )

        row = self.dofn.process(row_test, self.schema_dict_prospection).next()

        self.assertEqual(row['cst_sk'], 5013388254)
        self.assertEqual(row['cst_code'], '879304016')
        self.assertEqual(row['swp_sk'], 1)
        self.assertEqual(row['swp_code'], '[14],[27],[46]')
        self.assertEqual(row['time_sk'], 6752)
        self.assertEqual(row['sale_date'], '2015-06-17')
        self.assertEqual(row['swp_day_pdt_sent_nbr'], 0)
        self.assertEqual(row['swp_day_pdt_ret_nbr'], 1)
        self.assertEqual(row['swp_day_pdt_yr_purch_nbr'], 1)
        self.assertEqual(row['swp_day_pdt_yr_tvr'], 4.45)
        self.assertEqual(row['swp_day_care_tvr'], 0)
        self.assertEqual(row['system_last_update'], '2015-06-26 12:06:59')
        self.assertEqual(row['source_file_name'], 'SWP_ALZ_GCP_YRFR_D_DET.txt')
        self.assertEqual(row['processing_time'], '2019-06-24 11:40:29.962555 UTC')
        self.assertEqual(row['version_number'], '201905')


    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns(self):
        """
            test d'un enregistrement qui ne passe pas
            (brd_sk='1x' alors que cette zone est integer et cou_sk=2X)
        :return:
        """
        row_test = dict(
            cst_sk='501338825x',
            cst_code='879304016',
            swp_sk='1',
            swp_code='[14],[27],[46]',
            time_sk='6752x',
            sale_date='20150617',
            swp_day_pdt_sent_nbr='0',
            swp_day_pdt_ret_nbr='1',
            swp_day_pdt_yr_purch_nbr='1',
            swp_day_pdt_yr_tvr='4.45',
            swp_day_care_tvr='0.00',
            system_last_update='20150626 12:06:59',
            source_file_name='SWP_ALZ_GCP_YRFR_D_DET.txt',
            processing_time='2019-06-24 11:40:29.962555 UTC',
            version_number='201905'
        )

        # on teste si le retour dans le login comporte bien un message d'erreur

        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_prospection).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('cst_sk', '501338825x', 'time_sk', '6752x'), list_logs)

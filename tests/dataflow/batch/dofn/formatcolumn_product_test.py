'''
Module de test des classes et fonctions DoFn
'''
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn


class FormatColumnDoFnTest(TestCase):
    """
    Classe pour tester le formatage des donnees issues de la Raw Layer 2
    Dans le cas du fichier des articles on ne prend pas la totalite des zones
    de la table et du fichiers car il y en a plus de 450
    et seules 255 est permis dans un dictionnaire
    On ne teste donc que quelques zones String, Float, Integer et Date
    """

    def setUp(self):

        self.schema_dict_product = {'pf_pfproduitfiniid': 'STRING',
                                    'pf_pfproduitfiniidtech': 'STRING',
                                    'pf_pfproduitfininovers': 'INTEGER',
                                    'pf_statutplm': 'STRING',
                                    'pf_codeteinte': 'STRING',
                                    'pf_listepayscommercialisation': 'STRING',
                                    'pf_codeean': 'STRING',
                                    'processing_time': 'TIMESTAMP',
                                    'pf_prixcession': 'FLOAT',
                                    'pf_poidsbrut': 'FLOAT',
                                    'dp_contenancerevendiquee': 'INTEGER',
                                    'pf_datelancement': 'DATE',
                                    'pm_datefinversion': 'DATE'}
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_product(self):
        row_test = dict(pf_pfproduitfiniid='493v0',
                        pf_pfproduitfiniidtech='493',
                        pf_pfproduitfininovers='0',
                        pf_statutplm='Archive',
                        pf_codeteinte='',
                        pf_listepayscommercialisation='AT|BE|CH|DE|LU|NL',
                        pf_codeean='3660005629742',
                        pf_prixcession='1.03',
                        pf_poidsbrut='77.46',
                        dp_contenancerevendiquee='164',
                        pf_datelancement='01/04/2010',
                        pm_datefinversion='01/05/2015',
                        processing_time='2019-04-23 11:35:13.670687 UTC')

        row = self.dofn.process(row_test, self.schema_dict_product).next()
        self.assertEqual(row['pf_pfproduitfiniid'], '493v0')
        self.assertEqual(row['pf_pfproduitfiniidtech'], '493')
        self.assertEqual(row['pf_pfproduitfininovers'], 0)
        self.assertEqual(row['pf_statutplm'], 'Archive')
        self.assertEqual(row['pf_listepayscommercialisation'], 'AT|BE|CH|DE|LU|NL')
        self.assertEqual(row['pf_codeean'], '3660005629742')
        self.assertEqual(row['pf_prixcession'], 1.03)
        self.assertEqual(row['pf_poidsbrut'], 77.46)
        self.assertEqual(row['dp_contenancerevendiquee'], 164)
        self.assertEqual(row['pf_datelancement'], '2010-04-01')
        self.assertEqual(row['pm_datefinversion'], '2015-05-01')
        self.assertEqual(row['processing_time'], '2019-04-23 11:35:13.670687 UTC')

        # Tester si une valeur est vide

    def test_empty_values_product(self):
        """
         test de quelques zones qui sont vides
        :return:
        """
        row_test = dict(pf_pfproduitfiniid='493v0',
                        pf_pfproduitfiniidtech='493',
                        pf_pfproduitfininovers='0',
                        pf_statutplm='Archive',
                        pf_codeteinte='',
                        pf_listepayscommercialisation='AT|BE|CH|DE|LU|NL',
                        pf_codeean='3660005629742',
                        pf_prixcession='',
                        pf_poidsbrut='77.46',
                        dp_contenancerevendiquee='164',
                        pf_datelancement='',
                        pm_datefinversion='01/05/2015',
                        processing_time='2019-04-23 11:35:13.670687 UTC')

        row_test = self.dofn.process(row_test, self.schema_dict_product).next()

        for row in row_test:
            self.assertFalse('pf_codeteinte' in row)
            self.assertFalse('pf_prixcession' in row)
            self.assertFalse('pf_datelancement' in row)

        # tester si une valeur est au bon format

    def test_format_columns_product(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(pf_pfproduitfiniid='493v0',
                        pf_pfproduitfiniidtech='493',
                        pf_pfproduitfininovers='0',
                        pf_statutplm='Archive',
                        pf_codeteinte='1',
                        pf_listepayscommercialisation='AT|BE|CH|DE|LU|NL',
                        pf_codeean='3660005629742',
                        pf_prixcession='1.03',
                        pf_poidsbrut='77.46',
                        dp_contenancerevendiquee='164',
                        pf_datelancement='01/04/2010',
                        pm_datefinversion='01/05/2015',
                        processing_time='2019-04-23 11:35:13.670687 UTC')

        row_test = self.dofn.process(row_test, self.schema_dict_product).next()

        self.assertIsInstance(row_test['pf_pfproduitfiniid'], str)
        self.assertIsInstance(row_test['pf_pfproduitfiniidtech'], str)
        self.assertIsInstance(row_test['pf_pfproduitfininovers'], int)
        self.assertIsInstance(row_test['pf_codeteinte'], str)
        self.assertIsInstance(row_test['pf_statutplm'], str)
        self.assertIsInstance(row_test['pf_listepayscommercialisation'], str)
        self.assertIsInstance(row_test['pf_codeean'], str)
        self.assertIsInstance(row_test['pf_prixcession'], float)
        self.assertIsInstance(row_test['pf_poidsbrut'], float)
        self.assertIsInstance(row_test['dp_contenancerevendiquee'], int)
        self.assertIsInstance(row_test['pf_datelancement'], str)
        self.assertIsInstance(row_test['pm_datefinversion'], str)
        self.assertIsInstance(row_test['processing_time'], str)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_servi_header_parse_exception(self):

        """
            test de 2 zones qui ne passent pas
            (pf_prixcession='xxx' et dp_contenancerevendiquee='zzz',
            alors que ces champs sont en float)
        :return:
        """
        row_test = dict(pf_pfproduitfiniid='493v0',
                        pf_pfproduitfiniidtech='493',
                        pf_pfproduitfininovers='0',
                        pf_statutplm='Archive',
                        pf_codeteinte='1',
                        pf_listepayscommercialisation='AT|BE|CH|DE|LU|NL',
                        pf_codeean='3660005629742',
                        pf_prixcession='xxx',
                        pf_poidsbrut='77.46',
                        dp_contenancerevendiquee='zzz',
                        pf_datelancement='01/04/2010',
                        pm_datefinversion='01/05/2015',
                        processing_time='2019-04-23 11:35:13.670687 UTC')

        # on teste si y a  bien un message en cas de mauvais format des donnees
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_product).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}"'
                          ' with bad value "{}"'.format('pf_prixcession', 'xxx',
                                                        'dp_contenancerevendiquee',
                                                        'zzz'), list_logs)

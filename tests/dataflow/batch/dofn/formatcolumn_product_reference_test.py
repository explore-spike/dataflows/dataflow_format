"""
 Module de test des classes et fonctions DoFn
 Le fihcier doit etre nomme <nomfonction>_<typefichier>_test.py
"""
from unittest import TestCase
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn

from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_prod_ref = get_schema_from_json('resources/bq/DDL/schema/'
                                                         'product_reference.json')
        self.dofn = FormatColumnDoFn()
    # tester le bon format et les bonnes valeurs

    def test_values_columns_prod_ref(self):
        row_test = dict(nomtable='RfPays',
                        identifiant='BW',
                        libellelong_fr='BOSTWANA',
                        libellelong_en='',
                        libellecourt_fr='BW',
                        libellecourt_en='',
                        description_fr='',
                        description_en='',
                        idfamillegenerique='',
                        idsousfamillemaketing='',
                        idcategorie='',
                        idfranchise='',
                        quantite='',
                        quantite_us='',
                        unite='',
                        unite_us='',
                        nbjours='',
                        nbmois='',
                        localisationfournisseur_fr='',
                        localisationfournisseur_en='',
                        idpays='',
                        idlangues='',
                        idutilisateur='',
                        idpole='',
                        idgroupelangue='',
                        originepreferentielle='',
                        idgroupeorganisation='',
                        idclassifmarketing='',
                        sequencement='',
                        coefficient='',
                        idtypologiebavar1='',
                        libellelongmsehl_fr='',
                        libellelongmsehl_en='',
                        libellecourtmseht_fr='',
                        libellecourtmseht_en='',
                        libellecourtmseh3_fr='',
                        libellecourtmseh3_en='',
                        libellecourtmseh6_fr='',
                        libellecourtmseh6_en='',
                        idversionfranchise='',
                        statutdevie_fr='',
                        statutdevie_en='',
                        idgroupeproduit='',
                        idgroupefranchise='',
                        idusine='',
                        idbusinessrole='',
                        idgroupeautorisation='',
                        idfamillebusiness='',
                        source_file_name='20190411_TablesReferencesMDMProduits.csv',
                        processing_time='2019-04-23 13:28:08.068144 UTC')

        row = self.dofn.process(row_test, self.schema_dict_prod_ref).next()

        self.assertEqual(row['nomtable'], 'RfPays')
        self.assertEqual(row['identifiant'], 'BW')
        self.assertEqual(row['libellelong_fr'], 'BOSTWANA')
        self.assertEqual(row['libellecourt_fr'], 'BW')

        self.assertEqual(row['source_file_name'], '20190411_TablesReferencesMDMProduits.csv')
        self.assertEqual(row['processing_time'], '2019-04-23 13:28:08.068144 UTC')

    # Tester si une valeur est vide

    def test_empty_values_prod_ref(self):
        """

        :return:
        """
        row_test = dict(nomtable='RfPays',
                        identifiant='BW',
                        libellelong_fr='BOSTWANA',
                        libellelong_en='',
                        libellecourt_fr='BW',
                        libellecourt_en='',
                        description_fr='',
                        description_en='',
                        idfamillegenerique='',
                        idsousfamillemaketing='',
                        idcategorie='',
                        idfranchise='',
                        quantite='',
                        quantite_us='',
                        unite='',
                        unite_us='',
                        nbjours='',
                        nbmois='',
                        localisationfournisseur_fr='',
                        localisationfournisseur_en='',
                        idgroupeautorisation='',
                        idpays='',
                        idlangues='',
                        idutilisateur='',
                        idpole='',
                        idgroupelangue='',
                        originepreferentielle='',
                        idgroupeorganisation='',
                        idclassifmarketing='',
                        sequencement='',
                        coefficient='',
                        idtypologiebavar1='',
                        libellelongmsehl_fr='',
                        libellelongmsehl_en='',
                        libellecourtmseht_fr='',
                        libellecourtmseht_en='',
                        libellecourtmseh3_fr='',
                        libellecourtmseh3_en='',
                        libellecourtmseh6_fr='',
                        libellecourtmseh6_en='',
                        idversionfranchise='',
                        statutdevie_fr='',
                        statutdevie_en='',
                        idgroupeproduit='',
                        idgroupefranchise='',
                        idusine='',
                        idbusinessrole='',
                        idfamillebusiness='',
                        source_file_name='20190411_TablesReferencesMDMProduits.csv',
                        processing_time='2019-04-23 13:28:08.068144 UTC')

        row_test = self.dofn.process(row_test, self.schema_dict_prod_ref).next()

        # for row in row_test:
        self.assertFalse('idbusinessrole' in row_test)
        self.assertFalse('idclassifmarketing' in row_test)
        self.assertFalse('libellecourtmseht_fr' in row_test)
        self.assertFalse('idgroupeproduit' in row_test)
        self.assertFalse('idgroupefranchise' in row_test)
        self.assertFalse('idbusinessrole' in row_test)
        self.assertFalse('idfamillebusiness' in row_test)
        self.assertFalse('statutdevie_fr' in row_test)

        # tester si une valeur est au bon format

    def test_format_columns_prod_ref(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(nomtable='RfPays',
                        identifiant='BW',
                        libellelong_fr='BOSTWANA',
                        libellelong_en='',
                        libellecourt_fr='BW',
                        libellecourt_en='',
                        description_fr='',
                        description_en='',
                        idfamillegenerique='',
                        idsousfamillemaketing='',
                        idcategorie='',
                        idfranchise='',
                        quantite='',
                        quantite_us='',
                        unite='',
                        unite_us='',
                        nbjours='',
                        nbmois='',
                        localisationfournisseur_fr='',
                        localisationfournisseur_en='',
                        idpays='',
                        idlangues='',
                        idutilisateur='',
                        idpole='',
                        idgroupelangue='',
                        originepreferentielle='',
                        idgroupeorganisation='',
                        idclassifmarketing='',
                        sequencement='',
                        coefficient='',
                        idtypologiebavar1='',
                        libellelongmsehl_fr='',
                        libellelongmsehl_en='',
                        libellecourtmseht_fr='',
                        libellecourtmseht_en='',
                        libellecourtmseh3_fr='',
                        libellecourtmseh3_en='',
                        libellecourtmseh6_fr='',
                        libellecourtmseh6_en='',
                        idversionfranchise='',
                        idgroupeautorisation='',
                        statutdevie_fr='',
                        statutdevie_en='',
                        idgroupeproduit='',
                        idgroupefranchise='',
                        idusine='',
                        idbusinessrole='',
                        idfamillebusiness='',
                        source_file_name='20190411_TablesReferencesMDMProduits.csv',
                        processing_time='2019-04-23 13:28:08.068144 UTC')

        row_test = self.dofn.process(row_test, self.schema_dict_prod_ref).next()

        self.assertIsInstance(row_test['nomtable'], str)
        self.assertIsInstance(row_test['identifiant'], str)
        self.assertIsInstance(row_test['libellelong_fr'], str)
        self.assertIsInstance(row_test['libellecourt_fr'], str)

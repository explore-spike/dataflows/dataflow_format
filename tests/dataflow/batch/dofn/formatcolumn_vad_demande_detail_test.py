"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_detail = get_schema_from_json('resources/bq/DDL/'
                                                       'schema/sales_vad_demande_detail.json')
        self.dofn = FormatColumnDoFn()

    def test_values_columns_detail(self):
        """
        tester le bon format et les bonnes valeurs
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='1',
                        date_valeur='20190405',
                        ncli='001507974',
                        num_ordre='0000040',
                        type_ligne_code='1',
                        ligne_status='9',
                        origine_code='1',
                        structure_code='A',
                        pro_code='87028',
                        qty_order='001',
                        qty_fact='001',
                        catalog_price='0002500',
                        ca_ligne='0000000',
                        adv_typ='007',
                        use_code='2',
                        ccr_price='0001200',
                        source_file_name='servi_detail.txt.20190305052210',
                        processing_time='2019-04-08 14:57:34.142079 UTC',
                        version_number='00003262')

        row = self.dofn.process(row_test, self.schema_dict_detail).next()

        self.assertEqual(row['code_metier'], 'VAD')
        self.assertEqual(row['code_marque'], 'YR')
        self.assertEqual(row['code_pays'], 'FR')
        self.assertEqual(row['code_flux'], 1)
        self.assertEqual(row['date_valeur'], '2019-04-05')
        self.assertEqual(row['ncli'], '001507974')
        self.assertEqual(row['num_ordre'], 40)
        self.assertEqual(row['type_ligne_code'], '1')
        self.assertEqual(row['ligne_status'], '9')
        self.assertEqual(row['origine_code'], '1')
        self.assertEqual(row['structure_code'], 'A')
        self.assertEqual(row['pro_code'], '87028')
        self.assertEqual(row['qty_order'], 1)
        self.assertEqual(row['qty_fact'], 1)
        self.assertEqual(row['catalog_price'], 2500.0)
        self.assertEqual(row['ca_ligne'], 0.0)
        self.assertEqual(row['adv_typ'], 7)
        self.assertEqual(row['use_code'], '2')
        self.assertEqual(row['ccr_price'], 1200.00)
        self.assertEqual(row['source_file_name'], 'servi_detail.txt.20190305052210')
        self.assertEqual(row['processing_time'], '2019-04-08 14:57:34.142079 UTC')
        self.assertEqual(row['version_number'], 3262)

    # Tester si une valeur est vide
    def test_empty_values_detail(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190405',
                        ncli='001507974',
                        num_ordre='0000040',
                        type_ligne_code='1',
                        ligne_status='9',
                        origine_code='1',
                        structure_code='A',
                        pro_code='87028',
                        qty_order='001',
                        qty_fact='001',
                        catalog_price='0002500',
                        ca_ligne='0000000',
                        adv_typ='007',
                        use_code='2',
                        ccr_price='',
                        source_file_name='servi_detail.txt.20190305052210',
                        processing_time='2019-04-08 14:57:34.142079 UTC',
                        version_number='00003262')
        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('code_flux' in row)
        self.assertFalse('ccr_price' in row)

    # tester si toutes les valeurs sont au bon format
    def test_format_columns_detail(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='1',
                        date_valeur='20190405',
                        ncli='001507974',
                        num_ordre='0000040',
                        type_ligne_code='1',
                        ligne_status='9',
                        origine_code='1',
                        structure_code='A',
                        pro_code='87028',
                        qty_order='001',
                        qty_fact='001',
                        catalog_price='0002500',
                        ca_ligne='0000000',
                        adv_typ='007',
                        use_code='2',
                        ccr_price='0001200',
                        source_file_name='servi_detail.txt.20190305052210',
                        processing_time='2019-04-08 14:57:34.142079 UTC',
                        version_number='00003262')
        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester type conforme
        self.assertIsInstance(row['code_metier'], str)
        self.assertIsInstance(row['code_marque'], str)
        self.assertIsInstance(row['code_pays'], str)
        self.assertIsInstance(row['code_flux'], int)
        self.assertIsInstance(row['date_valeur'], str)
        self.assertIsInstance(row['ncli'], str)
        self.assertIsInstance(row['num_ordre'], int)
        self.assertIsInstance(row['type_ligne_code'], str)
        self.assertIsInstance(row['ligne_status'], str)
        self.assertIsInstance(row['origine_code'], str)
        self.assertIsInstance(row['structure_code'], str)
        self.assertIsInstance(row['pro_code'], str)
        self.assertIsInstance(row['qty_order'], int)
        self.assertIsInstance(row['qty_fact'], int)
        self.assertIsInstance(row['catalog_price'], float)
        self.assertIsInstance(row['ca_ligne'], float)
        self.assertIsInstance(row['adv_typ'], int)
        self.assertIsInstance(row['use_code'], str)
        self.assertIsInstance(row['ccr_price'], float)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['processing_time'], str)
        self.assertIsInstance(row['version_number'], int)

    def test_format_columns_detail_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (code_flux='X' alors que cette zone est integer et
            ccr_price='X00120X' alors que cette zone est float )
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='X',
                        date_valeur='20190405',
                        ncli='001507974',
                        num_ordre='0000040',
                        type_ligne_code='1',
                        ligne_status='9',
                        origine_code='1',
                        structure_code='A',
                        pro_code='87028',
                        qty_order='001',
                        qty_fact='001',
                        catalog_price='0002500',
                        ca_ligne='0000000',
                        adv_typ='007',
                        use_code='2',
                        ccr_price='X00120X',
                        source_file_name='servi_detail.txt.20190305052210',
                        processing_time='2019-04-08 14:57:34.142079 UTC',
                        version_number='00003262')

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_detail).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('code_flux', 'X', 'ccr_price', 'X00120X'), list_logs)

"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_detail = get_schema_from_json('resources/bq/DDL/schema/'
                                                       'customer_profilematching.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_detail(self):
        row_test = dict(
            id_mydear='04C87B82CFA58D83E053CD12A8C0BA79',
            id_channel='631440320',
            channel='VPM',
            email_sha256='471706d98ee81858f15c399960cfbfe52c79a8fe21d18b80f7efa0175ade206e',
            google_phone_sha256='5511016d4c7f577f7b15608eb45c67a01071531da5682c1725b3552748f57dd3',
            facebook_phone_sha256='235d71b99c6eafe'
                                  'ced4080543f8e2cf21c763bb6bd67cbb8ca650c79b03f0267',
            loyalty_card_sha256='2f67185b88c847dd6e5097313f8fad98a21ebb34d2f23ab846920c5e1ddaa310',
            source_file_name='20190505_17830_INFOS_CLIENTES_PROFILMATCHEES_extrait',
            processing_time='2019-05-13 15:31:20.955000'
        )

        row = self.dofn.process(row_test, self.schema_dict_detail).next()

        self.assertEqual(row['id_mydear'], '04C87B82CFA58D83E053CD12A8C0BA79')
        self.assertEqual(row['id_channel'], '631440320')
        self.assertEqual(row['channel'], 'VPM')
        self.assertEqual(row['email_sha256'], '471706d98ee81858'
                                              'f15c399960cfbfe52c'
                                              '79a8fe21d18b80f7efa0175ade206e')
        self.assertEqual(row['google_phone_sha256'], '5511016d'
                                                     '4c7f577f7b1'
                                                     '5608eb45c67a0107'
                                                     '1531da5682c1725b3552748f57dd3')
        self.assertEqual(row['facebook_phone_sha256'], '235d71b'
                                                       '99c6eafece'
                                                       'd4080543f8e2c'
                                                       'f21c763bb6bd67c'
                                                       'bb8ca650c79b03f0267')
        self.assertEqual(row['loyalty_card_sha256'], '2f67185b88c847dd6e5097'
                                                     '313f8fad98a21ebb34d2f2'
                                                     '3ab846920c5e1ddaa310')
        self.assertEqual(row['source_file_name'], '20190505_17830_INFOS_CLIENTES'
                                                  '_PROFILMATCHEES_extrait')
        self.assertEqual(row['processing_time'], '2019-05-13 15:31:20.955000')

    # Tester si une valeur est vide
    def test_empty_values_detail(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(
            id_mydear='04C87B82CFA58D83E053CD12A8C0BA79',
            id_channel='631440320',
            channel='',
            email_sha256='471706d98ee81858f15c399960cfbfe52c79a8fe21d18b80f7efa0175ade206e',
            google_phone_sha256='5511016d4c7f577f7b15608eb45c67a01071531da5682c1725b3552748f57dd3',
            facebook_phone_sha256='235d71b99c6e'
                                  'afeced4080543f8e2cf21c763bb6bd67cbb8ca650c79b03f0267',
            loyalty_card_sha256='2f67185b88c847dd6e5097313f8fad98a21ebb34d2f23ab846920c5e1ddaa310',
            source_file_name='20190505_17830_INFOS_CLIENTES_PROFILMATCHEES_extrait',
            processing_time='2019-05-13 15:31:20.955000'
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('channel' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_detail(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(
            id_mydear='04C87B82CFA58D83E053CD12A8C0BA79',
            id_channel='631440320',
            channel='VPM',
            email_sha256='471706d98ee81858f15c399960cfbfe52c79a8fe21d18b80f7efa0175ade206e',
            google_phone_sha256='5511016d4c7f577f7b15608eb45c67a01071531da5682c1725b3552748f57dd3',
            facebook_phone_sha256='235d71b99c6eafec'
                                  'ed4080543f8e2cf21c763bb6bd67cbb8ca650c79b03f0267',
            loyalty_card_sha256='2f67185b88c847dd6e5097313f8fad98a21ebb34d2f23ab846920c5e1ddaa310',
            source_file_name='20190505_17830_INFOS_CLIENTES_PROFILMATCHEES_extrait',
            processing_time='2019-05-13 15:31:20.955000'
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_detail)

        row = rows_generator.next()

        # Tester type conforme
        self.assertIsInstance(row['id_mydear'], str)
        self.assertIsInstance(row['id_channel'], str)
        self.assertIsInstance(row['channel'], str)
        self.assertIsInstance(row['email_sha256'], str)
        self.assertIsInstance(row['google_phone_sha256'], str)
        self.assertIsInstance(row['facebook_phone_sha256'], str)
        self.assertIsInstance(row['loyalty_card_sha256'], str)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['processing_time'], str)

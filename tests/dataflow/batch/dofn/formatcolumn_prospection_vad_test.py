"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer  2
    """

    def setUp(self):
        self.schema_dict_prospection = get_schema_from_json('resources/bq/DDL/schema/'
                                                       'prospection_vad.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row_test = dict(
            brd_sk='1',
            brd_code='YR',
            cou_sk='28',
            cou_code='FR',
            scd_sk='1',
            scd_code='VAD',
            creation_date='20110101',
            system_last_update='20110101',
            cst_sk='34883681',
            cst_code='368434787',
            key_sk='215497',
            key_code='0Q3K',
            cup_indx='105',
            cup_extract_date='20110215',
            time_sk='4490',
            time_date='20110309',
            source_file_name='PRS_DIA_GCP_YRFR_D_DET.txt',
            processing_time='2019-05-13 15:31:20.955000',
            version_number='201904'
        )

        row = self.dofn.process(row_test, self.schema_dict_prospection).next()

        self.assertEqual(row['brd_sk'], 1)
        self.assertEqual(row['brd_code'], 'YR')
        self.assertEqual(row['cou_sk'], 28)
        self.assertEqual(row['cou_code'], 'FR')
        self.assertEqual(row['scd_sk'], 1)
        self.assertEqual(row['scd_code'], 'VAD')
        self.assertEqual(row['creation_date'], '2011-01-01')
        self.assertEqual(row['system_last_update'], '2011-01-01')
        self.assertEqual(row['cst_sk'], 34883681)
        self.assertEqual(row['cst_code'], '368434787')
        self.assertEqual(row['key_sk'], 215497)
        self.assertEqual(row['key_code'], '0Q3K')
        self.assertEqual(row['cup_indx'], 105)
        self.assertEqual(row['cup_extract_date'], '2011-02-15')
        self.assertEqual(row['time_sk'], 4490)
        self.assertEqual(row['time_date'], '2011-03-09')
        self.assertEqual(row['source_file_name'], 'PRS_DIA_GCP_YRFR_D_DET.txt')
        self.assertEqual(row['processing_time'], '2019-05-13 15:31:20.955000')

    # Tester si une valeur est vide
    def test_empty_values(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(
            brd_sk='1',
            brd_code='',
            cou_sk='28',
            cou_code='FR',
            scd_sk='1',
            scd_code='VAD',
            creation_date='20110101',
            system_last_update='20110101',
            cst_sk='34883681',
            cst_code='368434787',
            key_sk='215497',
            key_code='0Q3K',
            cup_indx='105',
            cup_extract_date='20110215',
            time_sk='4490',
            time_date='20110309',
            source_file_name='PRS_DIA_GCP_YRFR_D_DET.txt',
            processing_time='2019-05-13 15:31:20.955000',
            version_number='201904'
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_prospection)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('brd_code' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns(self):
        """
            test d'un enregistrement qui ne passe pas
            (brd_sk='1x' alors que cette zone est integer et cou_sk=2X)
        :return:
        """
        row_test = dict(
            brd_sk='1x',
            brd_code='YR',
            cou_sk='2x',
            cou_code='FR',
            scd_sk='1',
            scd_code='VAD',
            creation_date='20110101',
            system_last_update='20110101',
            cst_sk='34883681',
            cst_code='368434787',
            key_sk='215497',
            key_code='0Q3K',
            cup_indx='105',
            cup_extract_date='20110215',
            time_sk='4490',
            time_date='20110309',
            source_file_name='PRS_DIA_GCP_YRFR_D_DET.txt',
            processing_time='2019-05-13 15:31:20.955000',
            version_number='201904')

        # on teste si le retour dans le login comporte bien un message d'erreur

        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_prospection).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('brd_sk', '1x', 'cou_sk', '2x'), list_logs)

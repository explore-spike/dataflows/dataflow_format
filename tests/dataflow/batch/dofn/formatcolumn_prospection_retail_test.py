"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
        84410|"7583"|31703|"4176"|5032909180|"605020658"|0|1|2|"[2],[99],[39]"|0|0|"19000101"|0|"19000101"|0|"19000101"|0.00|0.00|0|0.00|0|0.00|0|0.00|0|0.00|0.00|0.00000|0.00|0.00|0.00|"20150526 11:26:20"|0.399|0.399|0.000|0.00|0.000|0.000|0.00|0.00|6521|"20141113"|6521|"20141113"|0|0|0|1|0|0|0
    """

    def setUp(self):
        self.schema_dict_ = get_schema_from_json('resources/bq/DDL/schema/prospection_retail.json')

        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns(self):
        row_test = dict(
            key_sk="84410",
            key_code="7583",
            off_sk="31703",
            off_code="4176",
            cst_sk="5032909180",
            cst_code="605020658",
            fid_sk="0",
            cph_iteration_nb="1",
            swp_sk="2",
            swp_code="[2],[99],[39]",
            cph_ret_ind="0",
            cph_ret_time_sk="0",
            cph_ret_sale_date="19000101",
            cph_ret_last_time_sk="0",
            cph_ret_last_sale_date="19000101",
            cph_act_time_sk="0",
            cph_act_sale_date="19000101",
            cph_ret_pdt_yr_tvr_it="0.00",
            cph_ret_pdt_yr_tvr_ft="0.00",
            cph_ret_pdt_yr_qty="0",
            cph_ret_care_tvr="0.00",
            cph_ret_care_qty="0",
            cph_tot_pdt_yr_tvr="0.00",
            cph_tot_pdt_yr_qty="0",
            cph_tot_care_tvr="0.00",
            cph_tot_care_qty="0",
            cph_pro_mkt_cost="0.00",
            cph_fid_mkt_cost="0.00",
            cph_fid_invoiced="0.00000",
            cph_gift_mkt_cost="0.00",
            cph_gift_invoiced="0.00",
            cph_commission="0.00",
            system_last_update="20170309 14:08:20",
            cph_message_cost_befor_inv="0.399",
            cph_message_cost_after_inv="0.399",
            cph_fid_support_cost="0.000",
            cph_fid_services_cost="0.00",
            cph_misc_doc_cost_befor_inv="0.000",
            cph_misc_doc_cost_after_inv="0.000",
            cph_misc_samples_cost="0.00",
            cph_misc_services_cost="0.00",
            cph_first_depos_time_sk="6521",
            cph_first_depos_sale_date="20141113",
            cph_last_depos_time_sk="6521",
            cph_last_depos_sale_date="20141113",
            cph_fid_tot_pts_tvr="0",
            cph_fid_tot_pts_offer="0",
            cph_fid_tot_pts_partner="0",
            cph_cpt_off="1",
            cph_cpt_ret_gross="0",
            cph_cpt_ret_net="0",
            cph_cpt_ret_read="0",
            source_file_name="test",
            version_number="001",
            processing_time="20190614"
        )

        parsed_line = self.dofn.process(row_test, self.schema_dict_).next()
        print parsed_line

        self.assertEqual(parsed_line["key_sk"], 84410)
        self.assertEqual(parsed_line["key_code"], "7583")
        self.assertEqual(parsed_line["off_sk"], 31703)
        self.assertEqual(parsed_line["off_code"], "4176")
        self.assertEqual(parsed_line["cst_sk"], 5032909180)
        self.assertEqual(parsed_line["cst_code"], "605020658")
        self.assertEqual(parsed_line["fid_sk"], 0)
        self.assertEqual(parsed_line["cph_iteration_nb"], 1)
        self.assertEqual(parsed_line["swp_sk"], 2)
        self.assertEqual(parsed_line["swp_code"], "[2],[99],[39]")
        self.assertEqual(parsed_line["cph_ret_ind"], 0)
        self.assertEqual(parsed_line["cph_ret_time_sk"], 0)
        self.assertEqual(parsed_line["cph_ret_sale_date"], "1900-01-01")
        self.assertEqual(parsed_line["cph_ret_last_time_sk"], 0)
        self.assertEqual(parsed_line["cph_ret_last_sale_date"], "1900-01-01")
        self.assertEqual(parsed_line["cph_act_time_sk"], 0)
        self.assertEqual(parsed_line["cph_act_sale_date"], "1900-01-01")
        self.assertEqual(parsed_line["cph_ret_pdt_yr_tvr_it"], 0.00)
        self.assertEqual(parsed_line["cph_ret_pdt_yr_tvr_ft"], 0.00)
        self.assertEqual(parsed_line["cph_ret_pdt_yr_qty"], 0)
        self.assertEqual(parsed_line["cph_ret_care_tvr"], 0.00)
        self.assertEqual(parsed_line["cph_ret_care_qty"], 0)
        self.assertEqual(parsed_line["cph_tot_pdt_yr_tvr"], 0.00)
        self.assertEqual(parsed_line["cph_tot_pdt_yr_qty"], 0)
        self.assertEqual(parsed_line["cph_tot_care_tvr"], 0.00)
        self.assertEqual(parsed_line["cph_tot_care_qty"], 0)
        self.assertEqual(parsed_line["cph_pro_mkt_cost"], 0.00)
        self.assertEqual(parsed_line["cph_fid_mkt_cost"], 0.00)
        self.assertEqual(parsed_line["cph_fid_invoiced"], 0.00000)
        self.assertEqual(parsed_line["cph_gift_mkt_cost"], 0.00)
        self.assertEqual(parsed_line["cph_gift_invoiced"], 0.00)
        self.assertEqual(parsed_line["cph_commission"], 0.00)
        self.assertEqual(parsed_line["system_last_update"], "2017-03-09 14:08:20")
        self.assertEqual(parsed_line["cph_message_cost_befor_inv"], 0.399)
        self.assertEqual(parsed_line["cph_message_cost_after_inv"], 0.399)
        self.assertEqual(parsed_line["cph_fid_support_cost"], 0.000)
        self.assertEqual(parsed_line["cph_fid_services_cost"], 0.00)
        self.assertEqual(parsed_line["cph_misc_doc_cost_befor_inv"], 0.000)
        self.assertEqual(parsed_line["cph_misc_doc_cost_after_inv"], 0.000)
        self.assertEqual(parsed_line["cph_misc_samples_cost"], 0.00)
        self.assertEqual(parsed_line["cph_misc_services_cost"], 0.00)
        self.assertEqual(parsed_line["cph_first_depos_time_sk"], 6521)
        self.assertEqual(parsed_line["cph_first_depos_sale_date"], "2014-11-13")
        self.assertEqual(parsed_line["cph_last_depos_time_sk"], 6521)
        self.assertEqual(parsed_line["cph_last_depos_sale_date"], "2014-11-13")
        self.assertEqual(parsed_line["cph_fid_tot_pts_tvr"], 0)
        self.assertEqual(parsed_line["cph_fid_tot_pts_offer"], 0)
        self.assertEqual(parsed_line["cph_fid_tot_pts_partner"], 0)
        self.assertEqual(parsed_line["cph_cpt_off"], 1)
        self.assertEqual(parsed_line["cph_cpt_ret_gross"], 0)
        self.assertEqual(parsed_line["cph_cpt_ret_net"], 0)
        self.assertEqual(parsed_line["cph_cpt_ret_read"], 0)



    def test_format_columns(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(
            key_sk="84410",
            key_code="7583",
            off_sk="31703",
            off_code="4176",
            cst_sk="5032909180",
            cst_code="605020658",
            fid_sk="0",
            cph_iteration_nb="1",
            swp_sk="2",
            swp_code="[2],[99],[39]",
            cph_ret_ind="0",
            cph_ret_time_sk="0",
            cph_ret_sale_date="19000101",
            cph_ret_last_time_sk="0",
            cph_ret_last_sale_date="19000101",
            cph_act_time_sk="0",
            cph_act_sale_date="19000101",
            cph_ret_pdt_yr_tvr_it="0.00",
            cph_ret_pdt_yr_tvr_ft="0.00",
            cph_ret_pdt_yr_qty="0",
            cph_ret_care_tvr="0.00",
            cph_ret_care_qty="0",
            cph_tot_pdt_yr_tvr="0.00",
            cph_tot_pdt_yr_qty="0",
            cph_tot_care_tvr="0.00",
            cph_tot_care_qty="0",
            cph_pro_mkt_cost="0.00",
            cph_fid_mkt_cost="0.00",
            cph_fid_invoiced="0.00000",
            cph_gift_mkt_cost="0.00",
            cph_gift_invoiced="0.00",
            cph_commission="0.00",
            system_last_update="20150526 11:26:20",
            cph_message_cost_befor_inv="0.399",
            cph_message_cost_after_inv="0.399",
            cph_fid_support_cost="0.000",
            cph_fid_services_cost="0.00",
            cph_misc_doc_cost_befor_inv="0.000",
            cph_misc_doc_cost_after_inv="0.000",
            cph_misc_samples_cost="0.00",
            cph_misc_services_cost="0.00",
            cph_first_depos_time_sk="6521",
            cph_first_depos_sale_date="20141113",
            cph_last_depos_time_sk="6521",
            cph_last_depos_sale_date="20141113",
            cph_fid_tot_pts_tvr="0",
            cph_fid_tot_pts_offer="0",
            cph_fid_tot_pts_partner="0",
            cph_cpt_off="1",
            cph_cpt_ret_gross="0",
            cph_cpt_ret_net="0",
            cph_cpt_ret_read="0",
            source_file_name="test",
            version_number="001",
            processing_time="20190614"
        )

        rows_generator = self.dofn.process(row_test, self.schema_dict_)

        row = rows_generator.next()
        print self.schema_dict_
        # Tester type conforme
        self.assertIsInstance(row["key_sk"], int)
        self.assertIsInstance(row["key_code"], str)
        self.assertIsInstance(row["off_sk"], int)
        self.assertIsInstance(row["off_code"], str)
        # self.assertIsInstance(row["cst_sk"], int) Long, bien pris en compte par BQ, limitation P27
        self.assertIsInstance(row["cst_code"], str)
        self.assertIsInstance(row["fid_sk"], int)
        self.assertIsInstance(row["cph_iteration_nb"], int)
        self.assertIsInstance(row["swp_sk"], int)
        self.assertIsInstance(row["swp_code"], str)
        self.assertIsInstance(row["cph_ret_ind"], int)
        self.assertIsInstance(row["cph_ret_time_sk"], int)
        self.assertIsInstance(row["cph_ret_sale_date"], str)
        self.assertIsInstance(row["cph_ret_last_time_sk"], int)
        self.assertIsInstance(row["cph_ret_last_sale_date"], str)
        self.assertIsInstance(row["cph_act_time_sk"], int)
        self.assertIsInstance(row["cph_act_sale_date"], str)
        self.assertIsInstance(row["cph_ret_pdt_yr_tvr_it"], float)
        self.assertIsInstance(row["cph_ret_pdt_yr_tvr_ft"], float)
        self.assertIsInstance(row["cph_ret_pdt_yr_qty"], int)
        self.assertIsInstance(row["cph_ret_care_tvr"], float)
        self.assertIsInstance(row["cph_ret_care_qty"], int)
        self.assertIsInstance(row["cph_tot_pdt_yr_tvr"], float)
        self.assertIsInstance(row["cph_tot_pdt_yr_qty"], int)
        self.assertIsInstance(row["cph_tot_care_tvr"], float)
        self.assertIsInstance(row["cph_tot_care_qty"], int)
        self.assertIsInstance(row["cph_pro_mkt_cost"], float)
        self.assertIsInstance(row["cph_fid_mkt_cost"], float)
        self.assertIsInstance(row["cph_fid_invoiced"], float)
        self.assertIsInstance(row["cph_gift_mkt_cost"], float)
        self.assertIsInstance(row["cph_gift_invoiced"], float)
        self.assertIsInstance(row["cph_commission"], float)
        self.assertIsInstance(row["system_last_update"], str)
        self.assertIsInstance(row["cph_message_cost_befor_inv"], float)
        self.assertIsInstance(row["cph_message_cost_after_inv"], float)
        self.assertIsInstance(row["cph_fid_support_cost"], float)
        self.assertIsInstance(row["cph_fid_services_cost"], float)
        self.assertIsInstance(row["cph_misc_doc_cost_befor_inv"], float)
        self.assertIsInstance(row["cph_misc_doc_cost_after_inv"], float)
        self.assertIsInstance(row["cph_misc_samples_cost"], float)
        self.assertIsInstance(row["cph_misc_services_cost"], float)
        self.assertIsInstance(row["cph_first_depos_time_sk"], int)
        self.assertIsInstance(row["cph_first_depos_sale_date"], str)
        self.assertIsInstance(row["cph_last_depos_time_sk"], int)
        self.assertIsInstance(row["cph_last_depos_sale_date"], str)
        self.assertIsInstance(row["cph_fid_tot_pts_tvr"], int)
        self.assertIsInstance(row["cph_fid_tot_pts_offer"], int)
        self.assertIsInstance(row["cph_fid_tot_pts_partner"], int)
        self.assertIsInstance(row["cph_cpt_off"], int)
        self.assertIsInstance(row["cph_cpt_ret_gross"], int)
        self.assertIsInstance(row["cph_cpt_ret_net"], int)
        self.assertIsInstance(row["cph_cpt_ret_read"], int)

    def test_format_columns_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (psh_tva_prestation='B' alors que cette zone est float et
            psh_voucher_dscnt_rate='A' alors que cette zone est float )
        :return:
        """
        row_test = dict(
            key_sk="84410",
            key_code="7583",
            off_sk="31703",
            off_code="4176",
            cst_sk="5032909180",
            cst_code="605020658",
            fid_sk="0",
            cph_iteration_nb="1",
            swp_sk="2",
            swp_code="[2],[99],[39]",
            cph_ret_ind="0",
            cph_ret_time_sk="0",
            cph_ret_sale_date="19000101",
            cph_ret_last_time_sk="0",
            cph_ret_last_sale_date="19000101",
            cph_act_time_sk="0",
            cph_act_sale_date="19000101",
            cph_ret_pdt_yr_tvr_it="0.00",
            cph_ret_pdt_yr_tvr_ft="0.00",
            cph_ret_pdt_yr_qty="0",
            cph_ret_care_tvr="0.00",
            cph_ret_care_qty="0",
            cph_tot_pdt_yr_tvr="0.00",
            cph_tot_pdt_yr_qty="0",
            cph_tot_care_tvr="0.00",
            cph_tot_care_qty="0",
            cph_pro_mkt_cost="0.00",
            cph_fid_mkt_cost="0.00",
            cph_fid_invoiced="0.00000",
            cph_gift_mkt_cost="0.00",
            cph_gift_invoiced="A",
            cph_commission="0.00",
            system_last_update="20150526 11:26:20",
            cph_message_cost_befor_inv="0.399",
            cph_message_cost_after_inv="0.399",
            cph_fid_support_cost="0.000",
            cph_fid_services_cost="0.00",
            cph_misc_doc_cost_befor_inv="0.000",
            cph_misc_doc_cost_after_inv="0.000",
            cph_misc_samples_cost="0.00",
            cph_misc_services_cost="0.00",
            cph_first_depos_time_sk="6521",
            cph_first_depos_sale_date="20141113",
            cph_last_depos_time_sk="B",
            cph_last_depos_sale_date="20141113",
            cph_fid_tot_pts_tvr="0",
            cph_fid_tot_pts_offer="0",
            cph_fid_tot_pts_partner="0",
            cph_cpt_off="1",
            cph_cpt_ret_gross="0",
            cph_cpt_ret_net="0",
            cph_cpt_ret_read="0",
            source_file_name="test",
            version_number="001",
            processing_time="20190614"
        )

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}"'
                          ' with bad value "{}"'.format('cph_gift_invoiced',
                                                        'A', 'cph_last_depos_time_sk', 'B',
                                                        ), list_logs)

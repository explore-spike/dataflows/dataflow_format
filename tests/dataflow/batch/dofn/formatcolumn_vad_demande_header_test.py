"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn
from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_demande_header = get_schema_from_json('resources/bq/DDL/schema/'
                                                               'sales_vad_demande_header.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_demande_header(self):
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190404',
                        ncli='001802649',
                        ind_lot='7',
                        date='144',
                        code_lot='04576',
                        code_sequence='039',
                        typ_demande='1',
                        sous_plan='95037',
                        key_code='0032',
                        keypuncher_code='',
                        type_code='1',
                        date_arrivee='20190404',
                        num_ordre='0000040',
                        pay_type='1',
                        origin_code='3',
                        pay_montant='0000000',
                        ca_command='0007920',
                        ca_facture='0007920',
                        ca_avoir='0000000',
                        ca_reduction='0000000',
                        ca_force='0000000',
                        tax_fed='',
                        status_code='EX',
                        montant_commande='0001600',
                        ca_tax='0001320',
                        ca_port='0000000',
                        ca_total_hr='0015840',
                        hybris_order_id='000000000000',
                        source_file_name='demande_header.txt.20190410000000',
                        processing_time='2019-04-10 15:31:20.955000',
                        version_number='00003285')

        row = self.dofn.process(row_test, self.schema_dict_demande_header).next()

        self.assertEqual(row['code_metier'], 'VAD')
        self.assertEqual(row['code_marque'], 'YR')
        self.assertEqual(row['code_pays'], 'FR')
        self.assertEqual(row['date_valeur'], '2019-04-04')
        self.assertEqual(row['ncli'], '001802649')
        self.assertEqual(row['ind_lot'], '7')
        self.assertEqual(row['date'], '144')
        self.assertEqual(row['code_lot'], 4576)
        self.assertEqual(row['code_sequence'], 39)
        self.assertEqual(row['typ_demande'], '1')
        self.assertEqual(row['sous_plan'], '95037')
        self.assertEqual(row['key_code'], '0032')
        self.assertEqual(row['type_code'], '1')
        self.assertEqual(row['date_arrivee'], '2019-04-04')
        self.assertEqual(row['num_ordre'], 40)
        self.assertEqual(row['pay_type'], '1')
        self.assertEqual(row['origin_code'], '3')
        self.assertEqual(row['pay_montant'], 0.0)
        self.assertEqual(row['ca_command'], 7920.0)
        self.assertEqual(row['ca_facture'], 7920.0)
        self.assertEqual(row['ca_avoir'], 0.0)
        self.assertEqual(row['ca_reduction'], 0.0)
        self.assertEqual(row['ca_force'], 0.0)
        self.assertEqual(row['status_code'], 'EX')
        self.assertEqual(row['montant_commande'], 1600.0)
        self.assertEqual(row['ca_tax'], 1320.0)
        self.assertEqual(row['ca_port'], 0.0)
        self.assertEqual(row['ca_total_hr'], 15840.0)
        self.assertEqual(row['hybris_order_id'], '000000000000')
        self.assertEqual(row['source_file_name'], 'demande_header.txt.20190410000000')
        self.assertEqual(row['processing_time'], '2019-04-10 15:31:20.955000')
        self.assertEqual(row['version_number'], 3285)

    # Tester si une valeur est vide
    def test_empty_values_demande_header(self):
        """
            on teste les colonnes vides de l'enregistrement
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190404',
                        ncli='001802649',
                        ind_lot='7',
                        date='144',
                        code_lot='04576',
                        code_sequence='39',
                        typ_demande='1',
                        sous_plan='95037',
                        key_code='0032',
                        keypuncher_code='',
                        type_code='1',
                        date_arrivee='20190404',
                        num_ordre='0000040',
                        pay_type='1',
                        origin_code='3',
                        pay_montant='0000000',
                        ca_command='0007920',
                        ca_facture='0007920',
                        ca_avoir='0000000',
                        ca_reduction='0000000',
                        ca_force='0000000',
                        tax_fed='',
                        status_code='EX',
                        montant_commande='0001600',
                        ca_tax='0001320',
                        ca_port='0000000',
                        ca_total_hr='0015840',
                        hybris_order_id='000000000000',
                        source_file_name='demande_header.txt.20190410000000',
                        processing_time='2019-04-10 15:31:20.955000',
                        version_number='00003285')

        rows_generator = self.dofn.process(row_test, self.schema_dict_demande_header)

        row = rows_generator.next()

        # Tester Chaine vide
        self.assertFalse('code_flux' in row)
        self.assertFalse('keypuncher_code' in row)
        self.assertFalse('tax_fed' in row)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_demande_header(self):
        """
        on teste un enregistrement correct :
        toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190404',
                        ncli='001802649',
                        ind_lot='7',
                        date='144',
                        code_lot='04576',
                        code_sequence='039',
                        typ_demande='1',
                        sous_plan='95037',
                        key_code='0032',
                        keypuncher_code='',
                        type_code='1',
                        date_arrivee='20190404',
                        num_ordre='0000040',
                        pay_type='1',
                        origin_code='3',
                        pay_montant='0000000',
                        ca_command='0007920',
                        ca_facture='0007920',
                        ca_avoir='0000000',
                        ca_reduction='0000000',
                        ca_force='0000000',
                        tax_fed='',
                        status_code='EX',
                        montant_commande='0001600',
                        ca_tax='0001320',
                        ca_port='0000000',
                        ca_total_hr='0015840',
                        hybris_order_id='000000000000',
                        source_file_name='demande_header.txt.20190410000000',
                        processing_time='2019-04-10 15:31:20.955000',
                        version_number='00003285')

        rows_generator = self.dofn.process(row_test, self.schema_dict_demande_header)

        row = rows_generator.next()

        # Tester type conforme
        self.assertIsInstance(row['code_metier'], str)
        self.assertIsInstance(row['code_marque'], str)
        self.assertIsInstance(row['code_pays'], str)
        self.assertIsInstance(row['date_valeur'], str)
        self.assertIsInstance(row['ncli'], str)
        self.assertIsInstance(row['ind_lot'], str)
        self.assertIsInstance(row['date'], str)
        self.assertIsInstance(row['code_lot'], int)
        self.assertIsInstance(row['code_sequence'], int)
        self.assertIsInstance(row['typ_demande'], str)
        self.assertIsInstance(row['sous_plan'], str)
        self.assertIsInstance(row['key_code'], str)
        self.assertIsInstance(row['type_code'], str)
        self.assertIsInstance(row['date_arrivee'], str)
        self.assertIsInstance(row['num_ordre'], int)
        self.assertIsInstance(row['pay_type'], str)
        self.assertIsInstance(row['origin_code'], str)
        self.assertIsInstance(row['pay_montant'], float)
        self.assertIsInstance(row['ca_command'], float)
        self.assertIsInstance(row['ca_facture'], float)
        self.assertIsInstance(row['ca_avoir'], float)
        self.assertIsInstance(row['ca_reduction'], float)
        self.assertIsInstance(row['ca_force'], float)
        self.assertIsInstance(row['status_code'], str)
        self.assertIsInstance(row['montant_commande'], float)
        self.assertIsInstance(row['ca_tax'], float)
        self.assertIsInstance(row['ca_port'], float)
        self.assertIsInstance(row['ca_total_hr'], float)
        self.assertIsInstance(row['hybris_order_id'], str)
        self.assertIsInstance(row['source_file_name'], str)
        self.assertIsInstance(row['version_number'], int)

    def test_format_columns_demande_header_parse_exception(self):
        """
            test d'un enregistrement qui ne passe pas
            (date_valeur='A0190404' alors que cette zone est date et
            ind_lot='B' alors que cette zone est integer )
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='B0190404',
                        ncli='001802649',
                        ind_lot='7',
                        date='144',
                        code_lot='B4576',
                        code_sequence='039',
                        typ_demande='1',
                        sous_plan='95037',
                        key_code='0032',
                        keypuncher_code='',
                        type_code='1',
                        date_arrivee='20190404',
                        num_ordre='0000040',
                        pay_type='1',
                        origin_code='3',
                        pay_montant='0000000',
                        ca_command='0007920',
                        ca_facture='0007920',
                        ca_avoir='0000000',
                        ca_reduction='0000000',
                        ca_force='0000000',
                        tax_fed='',
                        status_code='EX',
                        montant_commande='0001600',
                        ca_tax='0001320',
                        ca_port='0000000',
                        ca_total_hr='0015840',
                        hybris_order_id='000000000000',
                        source_file_name='demande_header.txt.20190410000000',
                        processing_time='2019-04-10 15:31:20.955000',
                        version_number='00003285')

        # on teste si le retour dans le login comporte bien un message d'erreur
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_demande_header).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('date_valeur', 'B0190404', 'code_lot', 'B4576'), list_logs)

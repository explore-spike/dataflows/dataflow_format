"""
 Module de test des classes et fonctions DoFn
"""
from unittest import TestCase
from testfixtures import LogCapture
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn

from utils.utils import get_schema_from_json


class FormatColumnDoFnTest(TestCase):
    """
        Classe pour tester le formatage des donnees issues de la Raw Layer 2
    """

    def setUp(self):
        self.schema_dict_servi_header = get_schema_from_json('resources/bq/DDL/schema/'
                                                             'sales_vad_servi_header.json')
        self.dofn = FormatColumnDoFn()

    # tester le bon format et les bonnes valeurs
    def test_values_columns_servi_header(self):
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190304',
                        ncli='113901705',
                        typ_code='1',
                        code_sequence='32',
                        recycling_type='2',
                        montant_avoir='0.0',
                        montant_reduc='0.0',
                        montant_charge1='0.0',
                        montant_charge2='0.0',
                        montant_charge3='0.0',
                        montant_charge4='0.0',
                        seq_delivery='1',
                        montant_facture='0.0',
                        ca_ttc_yr='1150.0',
                        filtre_code='',
                        invoiced_code='',
                        colis_code='1',
                        shipping_date='20190304',
                        montant_remises='0.0',
                        ca_reduction='0.0',
                        distri_code='30',
                        pay_number='0',
                        num_order='0',
                        num_deliv='0',
                        tax_code='',
                        line_status='EX',
                        ind_lot='.',
                        creation_date='',
                        source_file_name='servi_header.txt.20190305052210',
                        processing_time='2019-04-09 13:28:08.068144 UTC',
                        version_number='00003262')

        row = self.dofn.process(row_test, self.schema_dict_servi_header).next()

        self.assertEqual(row['code_metier'], 'VAD')
        self.assertEqual(row['code_marque'], 'YR')
        self.assertEqual(row['code_pays'], 'FR')
        self.assertEqual(row['date_valeur'], '2019-03-04')
        self.assertEqual(row['ncli'], '113901705')
        self.assertEqual(row['typ_code'], '1')
        self.assertEqual(row['code_sequence'], 32)
        self.assertEqual(row['recycling_type'], '2')
        self.assertEqual(row['montant_avoir'], 0.0)
        self.assertEqual(row['montant_reduc'], 0.0)
        self.assertEqual(row['montant_charge1'], 0.0)
        self.assertEqual(row['montant_charge2'], 0.0)
        self.assertEqual(row['montant_charge3'], 0.0)
        self.assertEqual(row['montant_charge4'], 0.0)
        self.assertEqual(row['seq_delivery'], '1')
        self.assertEqual(row['montant_facture'], 0.0)
        self.assertEqual(row['ca_ttc_yr'], 1150.0)
        self.assertEqual(row['colis_code'], 1)
        self.assertEqual(row['shipping_date'], '2019-03-04')
        self.assertEqual(row['montant_remises'], 0.0)
        self.assertEqual(row['ca_reduction'], 0.0)
        self.assertEqual(row['distri_code'], '30')
        self.assertEqual(row['pay_number'], 0)
        self.assertEqual(row['num_order'], 0)
        self.assertEqual(row['num_deliv'], 0)
        self.assertEqual(row['line_status'], 'EX')
        self.assertEqual(row['ind_lot'], '.')
        self.assertEqual(row['source_file_name'], 'servi_header.txt.20190305052210')
        self.assertEqual(row['processing_time'], '2019-04-09 13:28:08.068144 UTC')
        self.assertEqual(row['version_number'], 3262)

        # Tester si une valeur est vide

    def test_empty_values_servi_header(self):
        """

        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190304',
                        ncli='113901705',
                        typ_code='1',
                        code_sequence='32',
                        recycling_type='2',
                        montant_avoir='0.0',
                        montant_reduc='0.0',
                        montant_charge1='0.0',
                        montant_charge2='0.0',
                        montant_charge3='0.0',
                        montant_charge4='0.0',
                        seq_delivery='1',
                        montant_facture='0.0',
                        ca_ttc_yr='1150.0',
                        filtre_code='',
                        invoiced_code='',
                        colis_code='1',
                        shipping_date='20190304',
                        montant_remises='0.0',
                        ca_reduction='0.0',
                        distri_code='30',
                        pay_number='0',
                        num_order='0',
                        num_deliv='0',
                        tax_code='',
                        line_status='EX',
                        ind_lot='.',
                        creation_date='',
                        source_file_name='servi_header.txt.20190305052210',
                        processing_time='2019-04-09 13:28:08.068144 UTC',
                        version_number='00003262')
        row_test = self.dofn.process(row_test, self.schema_dict_servi_header).next()

        for row in row_test:
            self.assertFalse('invoiced_code' in row)
            self.assertFalse('tax_code' in row)
            self.assertFalse('filtre_code' in row)

        # tester si une valeur est au bon format

    def test_format_columns_servi_header(self):
        """
            on teste un enregistrement correct :
            toutes les zones doivent avoir le format (string, int, float ... attendu)
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190304',
                        ncli='113901705',
                        typ_code='1',
                        code_sequence='32',
                        recycling_type='2',
                        montant_avoir='0.0',
                        montant_reduc='0.0',
                        montant_charge1='0.0',
                        montant_charge2='0.0',
                        montant_charge3='0.0',
                        montant_charge4='0.0',
                        seq_delivery='1',
                        montant_facture='0.0',
                        ca_ttc_yr='1150.0',
                        invoiced_code='',
                        filtre_code='',
                        colis_code='1',
                        shipping_date='20190304',
                        montant_remises='0.0',
                        ca_reduction='0.0',
                        distri_code='30',
                        pay_number='0',
                        num_order='0',
                        num_deliv='0',
                        tax_code='',
                        line_status='EX',
                        ind_lot='.',
                        creation_date='',
                        source_file_name='servi_header.txt.20190305052210',
                        processing_time='2019-04-09 13:28:08.068144 UTC',
                        version_number='00003262')

        row_test = self.dofn.process(row_test, self.schema_dict_servi_header).next()

        self.assertIsInstance(row_test['code_metier'], str)
        self.assertIsInstance(row_test['code_marque'], str)
        self.assertIsInstance(row_test['code_pays'], str)
        self.assertIsInstance(row_test['date_valeur'], str)
        self.assertIsInstance(row_test['ncli'], str)
        self.assertIsInstance(row_test['recycling_type'], str)
        self.assertIsInstance(row_test['montant_avoir'], float)
        self.assertIsInstance(row_test['montant_reduc'], float)
        self.assertIsInstance(row_test['montant_charge1'], float)
        self.assertIsInstance(row_test['montant_charge2'], float)
        self.assertIsInstance(row_test['montant_charge3'], float)
        self.assertIsInstance(row_test['montant_charge4'], float)
        self.assertIsInstance(row_test['seq_delivery'], str)
        self.assertIsInstance(row_test['montant_facture'], float)
        self.assertIsInstance(row_test['ca_ttc_yr'], float)
        self.assertIsInstance(row_test['shipping_date'], str)
        self.assertIsInstance(row_test['montant_remises'], float)
        self.assertIsInstance(row_test['ca_reduction'], float)
        self.assertIsInstance(row_test['distri_code'], str)
        self.assertIsInstance(row_test['num_deliv'], int)
        self.assertIsInstance(row_test['line_status'], str)
        self.assertIsInstance(row_test['source_file_name'], str)
        self.assertIsInstance(row_test['processing_time'], str)
        self.assertIsInstance(row_test['version_number'], int)

    # tester si une valeur est au mauvais format et afficher la cause d erreur
    def test_format_columns_servi_header_parse_exception(self):
        """
            test de 2 enregistrements qui ne passent pas
            (montant_avoir='zz' et montant_reduc='xx', alors que ces champs sont en float)
        :return:
        """
        row_test = dict(code_metier='VAD',
                        code_marque='YR',
                        code_pays='FR',
                        code_flux='',
                        date_valeur='20190304',
                        ncli='113901705',
                        typ_code='1',
                        code_sequence='32',
                        recycling_type='2',
                        montant_avoir='zz',
                        montant_reduc='xx',
                        montant_charge1='0.0',
                        montant_charge2='0.0',
                        montant_charge3='0.0',
                        montant_charge4='0.0',
                        seq_delivery='1',
                        montant_facture='0.0',
                        ca_ttc_yr='1150.0',
                        filtre_code='',
                        invoiced_code='',
                        colis_code='1',
                        shipping_date='20190304',
                        montant_remises='0.0',
                        ca_reduction='0.0',
                        distri_code='30',
                        pay_number='0',
                        num_order='0',
                        num_deliv='0',
                        tax_code='',
                        line_status='EX',
                        ind_lot='.',
                        creation_date='',
                        source_file_name='servi_header.txt.20190305052210',
                        processing_time='2019-04-09 13:28:08.068144 UTC',
                        version_number='00003262')

        # on teste si y a  bien un message en cas de mauvais format des donnees
        with LogCapture() as logs:
            self.dofn.process(row_test, self.schema_dict_servi_header).next()
            list_logs = [logrecord.msg for logrecord in logs.records]
            self.assertIn('Format error on column "{}" with bad value "{}" and '
                          'Format error on column "{}" with bad value "{}"'
                          .format('montant_avoir', 'zz', 'montant_reduc', 'xx'), list_logs)

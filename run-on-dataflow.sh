#!/usr/bin/env bash

#set PYTHON to your virtual env interpreter
PYTHON=venv/bin/python

PYTHON -m main \
        --project lab-discovery-0d2705
        --table_input lab-discovery-0d2705:rl_article.product_reference
        --table_output lab-discovery-0d2705:pl_article.pl_product_reference
        --source_file_name TablesReferencesMDMProduits.csv
        --bq_partition_date 2019-04-23
        --table_defect lab-discovery-0d2705:pl_article.pl_product_reference_defects
        --runner DataflowRunner
        --temp_location gs://staging-temp-bucket/temp
        --staging_location gs://staging-temp-bucket/staging
        --region europe-west1
        --zone europe-west1-b
        --setup_file ./setup.py


        --project lab-discovery-0d2705
        --table_input lab-discovery-0d2705:rl_sales.retail_detail
        --table_output lab-discovery-0d2705:pl_sales.retail_detail
        --source_file_name VTE_LYR_SOC_YRFR_D_DET_20190111010608.txt
        --bq_partition_date 2019-02-07
        --table_defect lab-discovery-0d2705:pl_sales.retail_detail_defects

PYTHON -m main \
    --project mydata-raw-yrfr-dev-601adc
    --table_input mydata-raw-yrfr-dev-601adc:rl2.calendar_retail
    --table_output mydata-preparedshare-595463:pl1.calendar_retail
    --source_file_name REF_TOS_LYR_YRFR_B_TOS_20190605145100
    --bq_partition_date 2019-06-06
    --runner DataflowRunner
    --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/
    --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/
    --region europe-west1
    --zone europe-west1-b
    --setup_file ./setup.py
    --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe
    --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com
    --table_defect mydata-preparedshare-595463:pl1_defects.customer_profilematching

#Trafic Retail
python main.py \
    --project mydata-raw-yrfr-dev-601adc \
    --table_input mydata-raw-yrfr-dev-601adc:rl2.traffic_retail \
    --table_output mydata-preparedshare-595463:pl1.traffic_retail \
    --source_file_name CPT_LYR_DAT_YRFR_V_CPT.TXT \
    --bq_partition_date 2019-06-11 \
    --runner DataflowRunner \
    --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
    --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
    --region europe-west1 \
    --zone europe-west1-b \
    --setup_file ./setup.py \
    --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
    --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
    --table_defect mydata-preparedshare-595463:pl1_defects.traffic_retail

#Prospection Retail
python main.py \
    --project mydata-raw-yrfr-dev-601adc \
    --table_input mydata-raw-yrfr-dev-601adc:rl2.prospection_retail \
    --table_output mydata-preparedshare-595463:pl1.prospection_retail \
    --source_file_name PRS_ALZ_GCP_YRFR_D_DET.txt \
    --bq_partition_date 2019-06-18 \
    --runner DataflowRunner \
    --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
    --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
    --region europe-west1 \
    --zone europe-west1-b \
    --setup_file ./setup.py \
    --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
    --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
    --table_defect mydata-preparedshare-595463:pl1_defects.prospection_retail

#LegacyAggMediaRetail
python main.py \
    --project mydata-raw-yrfr-dev-601adc \
    --table_input mydata-raw-yrfr-dev-601adc:rl2.legacy_agg_media_retail \
    --table_output mydata-preparedshare-595463:pl1.legacy_agg_media_retail \
    --source_file_name SWP_ALZ_GCP_YRFR_D_DET.txt \
    --bq_partition_date 2019-06-24 \
    --runner DataflowRunner \
    --job_name mydata-raw-yrfr-dev-rl2--pl1-legacy-agg-media-retail
    --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
    --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
    --region europe-west1 \
    --zone europe-west1-b \
    --setup_file ./setup.py \
    --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
    --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
    --table_defect mydata-preparedshare-595463:pl1_defects.legacy_agg_media_retail
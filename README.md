# TK-01172-dev-env

# Introduction 

C'est un programme utilisant Apache Beam (Python SDK) qui :
- Transfert de données brutes d'une table de la RL2 vers une table de la PL1
- Transforme les données String de la RL2 en données formatées dans la PL1
- Prends en compte dynamiquement la structure des tables en entrées et en sortie


# Running on GCP

### Authentication
```
gcloud auth application-default login
```
### Run on GCP

```
PYTHON -m main \
        --project PROJECT_ID
        --table_input PROJECT_SOURCE:DATASET_RL2.TABLE_PSD_RL2
        --table_output PROJECT_SINK:DATASET_PL1.TABLE_PSD_PL1
        --source_file_name SOURCE_FILE_NAME
        --bq_partition_date YYYY-MM-DD
        --table_defect PROJECT_SINK:DATASET_PL1.TABLE_PSD_DEFECTS_PL1 
        --runner DataflowRunner
        --temp_location TEMP_DIR
        --staging_location STAGING_DIR
        --region REGION
        --zone ZONE
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com
        --setup_file ./setup.py
```

### Run on local

```
PYTHON -m main \
        --project PROJECT_ID
        --table_input PROJECT_SOURCE:DATASET_RL2.TABLE_PSD_RL2
        --table_output PROJECT_SINK:DATASET_PL1.TABLE_PSD_PL1
        --source_file_name SOURCE_FILE_NAME
        --bq_partition_date YYYY-MM-DD
        --table_defect PROJECT_SINK:DATASET_PL1.TABLE_PSD_DEFECTS_PL1 
 
```


# Package and distribute sources to Google Cloud Composer
```
python setup.py sdist 
tar -xzvf dist/dataflow_batch_retail_rl2_pl1-0.1.0.tar.gz -C dist
gsutil -m cp -r dist/dataflow_batch_retail_rl2_pl1-0.1.0 gs://europe-west1-discovery-work-5620ac8f-bucket/dags

```

"""
Une implementation beam qui permet de lire les colonnes en string de la
table d'entree pour les mettre a leur format
attendu dans la table de destination   (string, int64, date ...)
"""

import logging
import re
from datetime import datetime
import apache_beam as beam
from apache_beam import pvalue
from utils.utils import encode_to_str_utf8


class FormatColumnDoFn(beam.DoFn):
    """
    L' implementation DoFn
    """

    def __init__(self):
        super(FormatColumnDoFn, self).__init__()

    def process(self, row, schema_dict):
        """
        formate les colonnes pour les mettre dans le format attendu (string, float64, int64 ....)
        :param row: a Dict representation of a row from Bigquery
        :param schema_dict: a Dict representation of
        :return: a Dict representation of the formatted row
        """

        # on boucle sur le schema pour verifier le formatage de chaque colonne
        output_line = {}
        errors = []

        for column_name, column_type in schema_dict.items():
            # formate chaque valeur de la ligne de donnees brutes et
            # recupere la valeur formatee et l'enventuel erreur
            formatted_value, error = format_value(row[column_name], column_name, column_type)

            # ajouter une nouvelle erreur a la liste des erreurs
            if error:
                errors.append(error)

            # si la valeur formatee n'est pas vide, elle est enregistree dans un dict
            if formatted_value is not None:
                output_line[column_name] = formatted_value
            else:
                logging.info('Empty value at column {}'.format(column_name))

        # si la liste des erreurs est vide, la ligne est correcte
        if not errors:
            # Tag OK par defaut
            yield output_line

        # sinon elle est rejete
        else:
            defect_reason = ' and '.join(errors)
            logging.error(defect_reason)

            defect_row = row
            defect_row['defect_reason'] = defect_reason

            # Tag KO
            yield pvalue.TaggedOutput('row_ko', defect_row)


def format_value(column_value, column_name, column_type):
    """
    Formate la valeur d'une colonne dans une ligne de donnees brutes selon le type cible
    :param column_value:
    :param column_name: nom de colonne : str
    :param column_type: type de donnees dans la colonne : str
    :return: value: valeur formatee d'une colonne dans la ligne: str or float or int
            error: erreur eventuelle lors du formatage: str
    """
    # si le nom de colonne n'est pas vide on renseigne les champs
    # en convertissant
    error = None
    value = None
    try:
        if column_value:
            if column_type == 'FLOAT':
                value = float(column_value)
            elif column_type == 'BOOLEAN':
                value = bool(column_value)
            elif column_type == 'INTEGER':
                value = int(column_value)
            elif column_type == 'DATE' or column_type == 'DATETIME':
                # si le format est dd/mm/yyyy ou mm/dd/yyyy
                if not (not re.match('[0-9]{2}/[0-9]{2}/[0-9]{4}', column_value)):
                    str_date = str(column_value)
                    # si le format est mm/dd/yyyy
                    # if int(str_date.split("/")[1]) > 12:
                    #     value = str(datetime.strptime(row[column_name], '%m/%d/%Y').date())
                    # # si le format est dd/mm/yyyy
                    # else:
                    value = str(datetime.strptime(column_value, '%d/%m/%Y').date())

                elif re.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{1,2}:[0-9]{2}:[0-9]{2}', column_value) is not None:
                    value = str(datetime.strptime(column_value, '%Y-%m-%d %H:%M:%S'))

                elif re.match(r'[0-9]{4}[0-9]{2}[0-9]{2} [0-9]{1,2}:[0-9]{2}:[0-9]{2}', column_value) is not None:
                    value = str(datetime.strptime(column_value, '%Y%m%d %H:%M:%S'))
                    print "OLALALALA"
                
                else:
                    value = str(datetime.strptime(column_value, '%Y%m%d').date())
            # pour les STRING
            else:
                value = encode_to_str_utf8(column_value)
        else:
            value = None
    except ValueError:
        error = 'Format error on column "{}" with bad value "{}"'.format(
            encode_to_str_utf8(column_name),
            encode_to_str_utf8(column_value))

    return value, error

"""
 Module focused on arguments parsing and pipeline options building
"""

from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions


class MyOptions(PipelineOptions):
    """
        Classe personnalise pour les parametres d'entrees de Dataflow
        pour les afficher dans la console Google
    """
    @classmethod
    def _add_argparse_args(cls, parser):
        # Nom complet de la table de depart
        parser.add_argument('--table_input',
                            dest='table_input',
                            required=True,
                            help='Full qualified name of the table input. Ex : '
                                 '(Project_id:dataset_id.table_id) ')

        # Nom complet de la table de depart
        parser.add_argument('--table_output',
                            dest='table_output',
                            required=True,
                            help='Full qualified name of the table output. '
                                 'Ex : (Project_id:dataset_id.table_id)')

        # nom du fichier en entree
        parser.add_argument('--source_file_name',
                            dest='source_file_name',
                            required=True,
                            default=None,
                            help='name of the source file name.')

        # date d'ingestion technique pour utiliser le partitionnement
        parser.add_argument('--bq_partition_date',
                            dest='bq_partition_date',
                            required=True,
                            help='technical date to use partition')

        # nom de la table d'arrivee (celle dans la quelle on ecrit)
        parser.add_argument('--table_defect',
                            dest='table_defect',
                            required=True,
                            help='Full qualified name of table defect '
                                 'Ex : (Project_id:dataset_id.table_id)')


def build_pipeline_options(argv):
    """
    Parse arguments
    :param argv: job arguments
    :return: parsed arguments end pipeline options
    """

    pipeline_options = MyOptions(argv)
    pipeline_options.view_as(SetupOptions).save_main_session = True

    return pipeline_options

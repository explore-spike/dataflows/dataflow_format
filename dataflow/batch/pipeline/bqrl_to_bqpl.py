"""
Module de conception de pipeline de transformation de donnees
"""
import apache_beam as beam
from apache_beam.io import WriteToBigQuery
from dataflow.batch.dofn.formatcolumn import FormatColumnDoFn

# recupere le schema de "config"
from dataflow.batch.config import config
from utils.utils import extract_schema, parse_schema_dict


def build_pipeline(pipeline_options):
    """
    cree un pipeline  qui lit une table big query de type "raw layer (rl)
    dont tout les champs sont en string
    pour les inserer dans un table big query de type "prepared layer (pl)
    dont les champs sont types (string, int, float)
    :param pipeline_options: arguments passed through the command line
    :param pipeline_options: built pipeline options
    :return: the pipeline to run
    """

    pipeline = beam.Pipeline(options=pipeline_options)

    # Read the table rows into a PCollection.
    # lit la table rl_detail et l'insere dans une PCollection
    #     dans "pipeline_options" on recupere les arguments definis dans "options"
    #  read : lance la requete "REQUEST" sur la table "table_input" du dataset "dataset_input"
    #         - la requete a besoin du parametre "source_file_name" pour lire les enregistrements
    #           appartenant
    #         a un nom de fichier
    # format_column : c'est cette fonction (FormatColumnDoFn) qui fait le split des colonnes
    # write : ecrit dans la tavle "table_output" du dataset "dataset_output"
    #         ecrit en APPEND et cree la table si besoin (ceci sera enleve pour la version finale)

    rows = (pipeline | 'Read RL2 BQ Data' >> beam.io.Read(beam.io.BigQuerySource(
        query=config.REQUEST.format(pipeline_options.table_input.replace(':', '.', 1),
                                    pipeline_options.source_file_name,
                                    pipeline_options.bq_partition_date),
        use_standard_sql=True))
            | 'Convert raw data to formatted data' >> beam.ParDo(
                FormatColumnDoFn(),
                parse_schema_dict(
                    extract_schema(pipeline_options.table_output)
                )
            ).with_outputs('row_ko', main='row_ok'))

    rows.row_ok | 'Write correct data to PL1 BQ' >> WriteToBigQuery(
        table=pipeline_options.table_output,
        create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)

    rows.row_ko | 'Write defects data to PL1 BQ' >> WriteToBigQuery(
        table=pipeline_options.table_defect,
        create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)

    return pipeline
